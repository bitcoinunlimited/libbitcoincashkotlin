package bitcoinunlimited.libbitcoincash

/**
 * A Bitcoin Cash contract is a designed to allow maximum arbitrary data
 * payload in the input spending from the contract
 *
 * This contract is described in:
 * https://nerdekollektivet.gitlab.io/votepeer-documentation/input-payload-contract.html
 */
class PayloadContract {
    companion object {

        const val MAX_PUSH1_SIZE = 520
        const val MAX_PUSH2_SIZE = 520
        const val MAX_PUSH3_SIZE = 463
        const val MAX_PAYLOAD_PER_INPUT_SIZE = MAX_PUSH1_SIZE + MAX_PUSH2_SIZE + MAX_PUSH3_SIZE
        const val REDEEMSCRIPT_PUBKEY_POS = 7
        const val REDEEMSCRIPT_PAYLOADHASH_POS = 46
        const val PAYLOAD_HASH_SIZE = 20

        /**
         * The redeemscript hashes the payload and public key, then verifies that the
         * hash matches the expected input. Then it verifies the transaction itself
         * (like a normal P2PKH tx would)
         */
        fun redeemScript(
            chain: ChainSelector,
            pubkey: ByteArray,
            payloadHash: ByteArray
        ): BCHscript {
            return BCHscript(
                chain,
                // hash push3
                OP.HASH160,

                // hash push2
                OP.SWAP,
                OP.HASH160,

                // concatenate hash of push3 and push2
                OP.CAT,

                // hash push1
                OP.SWAP,
                OP.HASH160,

                // concatenate hash1 to (push3 + push2)
                OP.CAT,

                // introduce the pubkey that will be required to spend the payload.
                // (the constant REDEEMSCRIPT_PUBKEY_POS should point to this push)
                OP.push(pubkey),
                // take extra copy of the pubkey for checksig later
                OP.DUP,

                // hash pubkey and concat to (push3 + push2 + push1 ..)
                OP.HASH160,
                // after ROT, the stack is (signature pubkey pushhashes pubkeyhash)
                OP.ROT,
                OP.CAT,

                // finally, the hash of hashes
                OP.HASH160,

                // compare it to he expected hash
                OP.push(payloadHash),
                OP.EQUALVERIFY,

                // check signature
                OP.CHECKSIGVERIFY,

                // verify that there are no additional inputs
                OP.DEPTH,
                OP.NOT
            )
        }

        // / The script that unlocks
        fun unlockingScript(
            chain: ChainSelector,
            signature: ByteArray,
            pubkey: ByteArray,
            payload: ByteArray
        ): BCHscript {

            if (signature.size != NetworkConstants.SCHNORR_SIGNATURE_SIZE) {
                error(
                    "Expected (Schnorr) signature to be %d bytes, got %d".format(
                        signature.size,
                        NetworkConstants.SCHNORR_SIGNATURE_SIZE
                    )
                )
            }

            val (push1, push2, push3) = payloadToStackItems(payload)

            return BCHscript(
                chain,
                OP.push(signature),
                OP.push(push1),
                OP.push(push2),
                OP.push(push3),
                OP.push(redeemScript(chain, pubkey, calcPayloadHash(pubkey, payload)).toByteArray())
            )
        }

        fun lockingScript(chain: ChainSelector, pubkey: ByteArray, payloadHash: ByteArray): BCHscript {
            return contractAddress(chain, pubkey, payloadHash).constraintScript()
        }

        fun contractAddress(chain: ChainSelector, pubkey: ByteArray, payloadHash: ByteArray): PayAddress {
            var hash = Hash.hash160(redeemScript(chain, pubkey, payloadHash).toByteArray())
            return PayAddress(chain, PayAddressType.P2SH, hash)
        }

        fun calcPayloadHash(pubkey: ByteArray, payload: ByteArray): ByteArray {
            if (pubkey.size != NetworkConstants.PUBLIC_KEY_COMPACT_SIZE) {
                error("Expected public key to be %d bytes, got %d".format(NetworkConstants.PUBLIC_KEY_COMPACT_SIZE, pubkey.size))
            }

            val (push1, push2, push3) = payloadToStackItems(payload)

            val push1h = Hash.hash160(push1)
            val push2h = Hash.hash160(push2)
            val push3h = Hash.hash160(push3)
            val pkh = Hash.hash160(pubkey)

            val concatenated = pkh + push3h + push2h + push1h

            return Hash.hash160(concatenated)
        }

        /**
         * Transform payload into items that fit on the Bitcoin Script stack.
         */
        fun payloadToStackItems(payload: ByteArray): Array<ByteArray> {
            if (payload.size > MAX_PAYLOAD_PER_INPUT_SIZE) {
                error("Too large payload (%d > %d)".format(payload.size, MAX_PAYLOAD_PER_INPUT_SIZE))
            }

            if (payload.isEmpty()) {
                error("Payload cannot be empty")
            }

            val PUSH2_OFFSET = MAX_PUSH1_SIZE
            val PUSH3_OFFSET = MAX_PUSH1_SIZE + MAX_PUSH2_SIZE

            val push1 = payload.copyOfRange(0, Math.min(MAX_PUSH1_SIZE, payload.size))
            var push2 = ByteArray(0)
            var push3 = ByteArray(0)

            if (payload.size > PUSH2_OFFSET) {
                var toIndex = PUSH3_OFFSET
                if (toIndex > payload.size) {
                    toIndex = payload.size
                }
                push2 = payload.copyOfRange(PUSH2_OFFSET, toIndex)
            }

            if (payload.size > PUSH3_OFFSET) {
                var toIndex = PUSH3_OFFSET + MAX_PUSH3_SIZE
                if (toIndex > payload.size) {
                    toIndex = payload.size
                }
                push3 = payload.copyOfRange(PUSH3_OFFSET, toIndex)
            }

            return arrayOf(push1, push2, push3)
        }

        /**
         * Read a payload from a transaction input.
         */
        fun getPayloadFromInputScript(
            chain: ChainSelector,
            script: BCHscript,
            validate: Boolean
        ): ByteArray {

            // First 65 bytes should be a signature
            val (signature, sigRange) = script.copyFront()
            if (signature.size != NetworkConstants.SCHNORR_SIGNATURE_SIZE) {
                throw IllegalArgumentException(
                    (
                        "Expected signature of size ${NetworkConstants.SCHNORR_SIGNATURE_SIZE}, " +
                            "but got stack item of size ${signature.size}"
                        )
                )
            }
            // Payload Pushes
            val (push1, push1Range) = script.copyStackElementAt(sigRange.last + 1)
            val (push2, push2Range) = script.copyStackElementAt(push1Range.last + 1)
            val (push3, push3Range) = script.copyStackElementAt(push2Range.last + 1)

            val payload = push1 + push2 + push3
            if (validate) {
                val (redeemScript, redeemScriptRange) = script.copyStackElementAt(push3Range.last + 1)

                // There should be no more elements on the stack.
                if (script.toByteArray().size > redeemScriptRange.last + 1) {
                    throw IllegalArgumentException("Too many stack elements in scriptSig")
                }

                validateRedeemScript(chain, BCHscript(chain, redeemScript), payload)
            }
            return payload
        }

        /**
         * Read payload from all inputs
         */
        fun getPayloadFromTx(
            chain: ChainSelector,
            tx: BCHtransaction,
            validate: Boolean = true
        ): ByteArray {

            if (tx.inputs.isEmpty()) {
                throw IllegalArgumentException("Transaction has no inputs")
            }

            // We require the first input to contain a payload, so we want it
            // to throw on error (no try/catch)
            var payload = getPayloadFromInputScript(chain, tx.inputs[0].script, validate)

            // Using more inputs to deliver a payload is optional.
            for (i in 1 until tx.inputs.size) {
                try {
                    payload += getPayloadFromInputScript(chain, tx.inputs[i].script, validate)
                } catch (e: IllegalArgumentException) { // Ignoring inputs and all subsequent inputs.
                    return payload
                }
            }
            return payload
        }

        /**
         * Validates that:
         * - Script is in fact this contract
         * - Payload hash matches payload and pubkey
         * - (NYI: Signature is valid for transaction)
         *
         * Throws on validation error
         */
        fun validateRedeemScript(
            chain: ChainSelector,
            script: BCHscript,
            payload: ByteArray
        ) {

            // Read the pubkey
            val (pubkey,) = script.copyStackElementAt(REDEEMSCRIPT_PUBKEY_POS)
            if (pubkey.size != NetworkConstants.PUBLIC_KEY_COMPACT_SIZE) {
                val err = (
                    "Found a push operation in the redeemscript where pubkey " +
                        "is expected, but size of push was ${pubkey.size}, " +
                        "expected ${NetworkConstants.PUBLIC_KEY_COMPACT_SIZE}" +
                        " ${script.toHex()}"
                    )
                throw IllegalArgumentException(err)
            }
            // Read the payload cash
            val (payloadHash,) = script.copyStackElementAt(REDEEMSCRIPT_PAYLOADHASH_POS)
            if (payloadHash.size != PAYLOAD_HASH_SIZE) {
                val err = (
                    "Found a push operation in the redeemscript where payload " +
                        "hash is expected, but size of push was " +
                        "${payloadHash.size}, expected $PAYLOAD_HASH_SIZE"
                    )
                throw IllegalArgumentException(err)
            }

            // Verify that it's the exact redeem script we expect
            val expectedScript = redeemScript(chain, pubkey, payloadHash)

            if (!script.contentEquals(expectedScript)) {
                throw IllegalArgumentException("Incorrect redeemscript for payload contract")
            }

            // Verify for correct payload hash
            val h = calcPayloadHash(pubkey, payload)
            if (!h.contentEquals(payloadHash)) {
                throw IllegalArgumentException("Payload Hash mismatch")
            }

            // TODO: Verify transaction signature. We have pubkey, signature and tx available.
        }
    }
}