package bitcoinunlimited.libbitcoincash.vote

import bitcoinunlimited.libbitcoincash.*

/**
 * https://docs.voter.cash/multi-option-vote-contract/
 */
class MultiOptionVote(
    val salt: ByteArray,
    val description: String,
    val beginHeight: Int,
    val endHeight: Int,
    val voteOptions: Array<String>,
    participantsIn: Array<ByteArray>,
) {
    val participants = UtilVote.sortByteArray(participantsIn)
    val voteOptionsHashed = UtilVote.sortByteArray(voteOptions.map { UtilVote.hash160Salted(salt, it.toByteArray()) }.toTypedArray())

    init {

        val correctSizedPKH = {
            var ok = true
            for (p in participants) {
                if (p.size != NetworkConstants.PUBLIC_KEY_HASH_SIZE) {
                    ok = false
                    break
                }
            }
            ok
        }

        val hasDuplicates = {
            // Assumes sorted list
            var foundDuplicate = false
            var prev: ByteArray? = null
            for (p in participants) {
                prev?.let {
                    if (p.contentEquals(prev)) {
                        foundDuplicate = true
                        return@let
                    }
                }
                prev = p
            }
            foundDuplicate
        }

        require(beginHeight >= 0) { "Begin height must be a positive number (was $beginHeight)" }
        require(endHeight >= beginHeight) { "End height must be larger or equal to begin height ($endHeight < $beginHeight)" }
        require(salt.isNotEmpty()) { "Salt is empty" }
        require(voteOptionsHashed.isNotEmpty()) { "No vote options" }
        require(participants.isNotEmpty()) { "No participants" }
        require(correctSizedPKH()) { "Incorrectly sized pubkeys" }
        require(!hasDuplicates()) { "Cannot have duplicate participants" }
    }

    /**
     * A participant merkle tree looks like this:
     *                root
     *              /     \
     *             N      N
     *           / |      | \
     *         H   H      H  Ø
     *         |   |      |
     *        P1  P2     P3
     */
    fun participantsMerkleRootHash(): ByteArray {
        if (participants.isEmpty()) {
            error("Participants cannot be empty")
        }

        // Wrap leafs
        var queue: MutableList<ByteArray> = participants.map { Hash.sha256(it) }.toMutableList()

        // Special case: If there is only 1 participant leaf, we need to pair
        // it with a Ø-node.
        //
        // Without this, the node would be the merkle root and we would
        // not be able to generate "proof of number of participants".
        if (queue.size == 1) {
            queue.add(NULL_HASH)
        }

        val createParents = {
            assert(queue.size % 2 == 0)
            var parents: MutableList<ByteArray> = mutableListOf()

            while (!queue.isEmpty()) {
                val left = queue.removeFirst()
                val right = queue.removeFirst()
                parents.add(Hash.sha256(left + right))
            }
            parents
        }

        while (queue.size != 1) {
            if (queue.size % 2 != 0) {
                // Push a NULL node to balance all participants to same level.
                queue.add(NULL_HASH)
            }
            queue = createParents()
        }
        return queue.first()
    }

    /**
     * The Election ID consist of a hash of all the data belonging to a vote.
     */
    fun electionId(): ByteArray {
        var voteOptionsCombined = ByteArray(0)

        for (o in UtilVote.sortByteArray(voteOptionsHashed)) {
            if (o.size != VOTE_OPTION_LEN) {
                throw IllegalArgumentException("Vote option hash must be ${RingSignatureVoteBase.VOTE_OPTION_LEN} bytes (got ${o.size})")
            }
            voteOptionsCombined += o
        }
        val electionIDParts: Array<ByteArray> = arrayOf(
            salt,
            description.toByteArray(),
            UtilVote.longToUInt32ByteArray(beginHeight.toLong()),
            UtilVote.longToUInt32ByteArray(endHeight.toLong()),
            voteOptionsCombined,
            participantsMerkleRootHash()
        )

        var electionID = ByteArray(0)
        for (p in electionIDParts) {
            electionID += p
        }

        return Hash.hash160(electionID)
    }

    fun voterContractAddress(
        chain: ChainSelector,
        pubkeyHash: ByteArray
    ): PayAddress {

        val hash = Hash.hash160(redeemScript(chain, pubkeyHash).flatten())
        return PayAddress(chain, PayAddressType.P2SH, hash)
    }

    fun redeemScript(
        chain: ChainSelector,
        pubkeyHash: ByteArray,
    ): BCHscript {
        return redeemScript(chain, pubkeyHash, electionId())
    }

    fun signVote(
        voterSecret: Secret,
        hashedVoteOption: ByteArray,
    ): Pair<ByteArray, ByteArray> {
        if (!UtilVote.byteArrayContains(voteOptionsHashed, hashedVoteOption)) {
            error("Invalid vote option for election.")
        }
        val message = electionId() + hashedVoteOption
        assert(message.size == ELECTION_ID_LEN + VOTE_OPTION_LEN)
        val signature = Key.signDataUsingSchnorr(message, voterSecret.getSecret())
        return Pair(message, signature)
    }

    /**
     * Returns a transaction that casts the vote.
     *
     * The input needs to be a utxo from this contract. The input needs to have
     * the private key for signing the vote in `input.spendable.secret`.
     */
    fun castVote(
        chain: ChainSelector,
        input: BCHinput,
        changeAddress: PayAddress,
        voteOptionHash: ByteArray
    ): BCHtransaction {

        if (!UtilVote.byteArrayContains(voteOptionsHashed, voteOptionHash)) {
            throw IllegalArgumentException("Invalid vote option")
        }

        if (castingTxSize(chain) > input.spendable.amount) {
            throw IllegalArgumentException("Input amount too small")
        }

        val inputSecret = input.spendable.secret ?: throw IllegalArgumentException("input secret missing")
        val pubkey = PayDestination.GetPubKey(inputSecret.getSecret())
        val pubkeyHash = Hash.hash160(pubkey)

        input.spendable.priorOutScript = BCHscript(chain, redeemScript(chain, pubkeyHash).toByteArray())

        val tx = BCHtransaction(chain)
        tx.inputs.add(input)

        val changeOutput = BCHoutput(chain)
        changeOutput.amount = input.spendable.amount - (castingTxSize(chain) - NetworkConstants.DEFAULT_DUST_THRESHOLD)
        changeOutput.script = changeAddress.outputScript()
        tx.outputs.add(changeOutput)

        if (changeOutput.amount < NetworkConstants.DEFAULT_DUST_THRESHOLD) {
            throw IllegalStateException("Change output is less than dust")
        }

        val txSig = UtilVote.signInput(tx, 0)
        val (_, voteSignature) = signVote(inputSecret, voteOptionHash)

        val castVoteScript = inputScript(chain, pubkey, voteOptionHash, txSig, voteSignature)
        tx.inputs[0].script = castVoteScript

        if (tx.feeRate < NetworkConstants.MIN_TX_FEE_RATE) {
            throw IllegalStateException("Vote casting transaction has too low fee rate ${tx.feeRate} < ${NetworkConstants.MIN_TX_FEE_RATE})")
        }

        return tx
    }

    /**
     * Script to cast a vote (spend from contract).
     */
    fun inputScript(
        chain: ChainSelector,
        voterPubkey: ByteArray,
        voteOptionHash: ByteArray,
        txSignature: ByteArray,
        voteSignature: ByteArray
    ): BCHscript {
        return inputScript(chain, voterPubkey, voteOptionHash, txSignature, voteSignature, electionId())
    }

    companion object {
        val CAST_FEE = 1000
        val NULL_HASH = ByteArray(32)
        val ELECTION_ID_LEN = 20
        val VOTE_OPTION_LEN = 20
        val VOTE_SIGNATURE_LEN = 64

        /**
         * Parse the vote from a multi-option-vote transaction.
         *
         * Does not validate if it's a vote contract, nor does it validate the signature!
         */
        fun parseVote(tx: BCHtransaction, validHashedVoteOptions: Array<ByteArray>, inputIndex: Int = 0): ByteArray {
            if (tx.inputs.size < inputIndex + 1) {
                throw IndexOutOfBoundsException("tx.inputs.size < inputIndex + 1")
            }
            val script = tx.inputs[inputIndex].script.flatten()
            var pos = 0
            if (script.isEmpty())
                throw IllegalStateException("tx.inputs[0].script is an empty array")

            val voteSignatureSize = script[pos++].toInt()
            if (voteSignatureSize != VOTE_SIGNATURE_LEN) {
                throw IllegalStateException("Expected vote signature of size $VOTE_SIGNATURE_LEN, found something of size $voteSignatureSize")
            }
            // skip signature
            pos += voteSignatureSize
            val electionIDSize = script[pos++].toInt()
            if (electionIDSize != ELECTION_ID_LEN) {
                throw IllegalStateException("Expected election ID of size $VOTE_SIGNATURE_LEN, found something of size $electionIDSize")
            }
            // skip ID
            pos += electionIDSize

            val voteOptionHashLen = script[pos++].toInt()
            if (voteOptionHashLen != VOTE_OPTION_LEN) {
                throw IllegalStateException("Expected election ID of size $VOTE_OPTION_LEN, found something of size $voteOptionHashLen")
            }
            val vote = script.copyOfRange(pos, pos + voteOptionHashLen)
            if (!UtilVote.byteArrayContains(validHashedVoteOptions, vote)) {
                throw IllegalStateException("Invalid vote option in transaction")
            }
            return vote
        }

        /**
         * Size of a transaction casting a vote with multi-option-vote,
         * assuming one P2PKH output.
         */
        fun castingTxSize(chain: ChainSelector): Long {

            val inputScriptSize = inputScript(
                chain,
                ByteArray(NetworkConstants.PUBLIC_KEY_COMPACT_SIZE),
                ByteArray(VOTE_OPTION_LEN),
                ByteArray(NetworkConstants.SCHNORR_SIGNATURE_SIZE),
                ByteArray(VOTE_SIGNATURE_LEN),
                ByteArray(ELECTION_ID_LEN)
            ).size

            return (
                UtilVote.getBaseTxSize(1, 1) +
                    NetworkConstants.EMPTY_TX_INPUT_SIZE +
                    1 + /* extra byte for scriptsig varint */
                    inputScriptSize +
                    NetworkConstants.P2PKH_OUTPUT_SIZE +
                    NetworkConstants.DEFAULT_DUST_THRESHOLD
                ).toLong() /* dust for the output */
        }

        /**
         * The multi-option-vote Script.
         */
        fun redeemScript(
            chain: ChainSelector,
            pubkeyHash: ByteArray,
            electionID: ByteArray,
        ): BCHscript {
            if (pubkeyHash.size != NetworkConstants.PUBLIC_KEY_HASH_SIZE) {
                error("Pubkey hash should be 20 bytes")
            }
            if (electionID.size != ELECTION_ID_LEN) {
                error("Election ID should be 20 bytes")
            }
            return BCHscript(
                chain,
                OP.OVER,
                OP.CHECKSIGVERIFY,
                OP.DUP,
                OP.HASH160,
                OP.push(pubkeyHash),
                OP.EQUALVERIFY,
                OP.push(2),
                OP.PICK,
                OP.push(electionID),
                OP.EQUALVERIFY,
                OP.ROT,
                OP.ROT,
                OP.CAT,
                OP.SWAP,
                OP.CHECKDATASIGVERIFY,
                OP.DEPTH,
                OP.NOT
            )
        }

        /**
         * Script to cast a vote (spend from contract).
         */
        fun inputScript(
            chain: ChainSelector,
            voterPubkey: ByteArray,
            voteOptionHash: ByteArray,
            txSignature: ByteArray,
            voteSignature: ByteArray,
            electionID: ByteArray
        ): BCHscript {
            assert(voterPubkey.size == NetworkConstants.PUBLIC_KEY_COMPACT_SIZE)
            assert(voteOptionHash.size == VOTE_OPTION_LEN)
            assert(txSignature.size == NetworkConstants.SCHNORR_SIGNATURE_SIZE)
            assert(voteSignature.size == VOTE_SIGNATURE_LEN)
            assert(electionID.size == ELECTION_ID_LEN)

            return BCHscript(
                chain,
                OP.push(voteSignature),
                OP.push(electionID),
                OP.push(voteOptionHash),
                OP.push(voterPubkey),
                OP.push(txSignature),
                OP.push(redeemScript(chain, Hash.hash160(voterPubkey), electionID).flatten())
            )
        }
    }
}