package bitcoinunlimited.libbitcoincash

/**
 * Sign data (compatible with OP_CHECKDATASIG)
 */
class Key {
    companion object {
        /**
         * Sign data (OP_CHECKDATASIG compatible)
         */
        @JvmStatic
        external fun signDataUsingSchnorr(data: ByteArray, secret: ByteArray): ByteArray

        /**
         * Decode private key (WIF format)
         */
        @JvmStatic
        external fun decodePrivateKey(chainSelector: Byte, wifKey: String): ByteArray
    }
}