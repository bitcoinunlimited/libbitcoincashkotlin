// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package bitcoinunlimited.libbitcoincash

import kotlinx.coroutines.*

import java.net.*
import java.io.*
import java.nio.charset.Charset
//import java.time.Instant
import java.util.Date
import java.util.concurrent.Executors
import kotlin.concurrent.thread
import kotlin.coroutines.CoroutineContext
import kotlin.math.min
import kotlin.time.DurationUnit
import kotlin.time.TimeMark
import kotlin.time.ExperimentalTime
import kotlin.time.TimeSource

private val LogIt = GetLog("BU.p2p")

val mRBCHPort = 7227 // For configurations where both testnet and regtest is run
val BCHtestnetPort = 18333
val BCHregtestPort = 18444
val BCHregtest2Port = 8334  // for multicurrency tests, you can create 2 separate regtest chains on different ports
val BCHmainnetPort = 8333
val BCHnolnetPort = 9333
val NextChainPort = 7228

//val CASH_MAGIC_BYTES = hashMapOf("mainnet" to byteArrayOf(0xe3.toByte(),0xe1.toByte(),0xf3.toByte(),0xe8.toByte() ) )

val BCH_CASH_MAGIC_TESTNET3 = byteArrayOf(0xf4.toByte(), 0xe5.toByte(), 0xf3.toByte(), 0xf4.toByte())
val BCH_CASH_MAGIC_REGTEST = byteArrayOf(0xda.toByte(), 0xb5.toByte(), 0xbf.toByte(), 0xfa.toByte())
val BCH_CASH_MAGIC_MAINNET = byteArrayOf(0xe3.toByte(), 0xe1.toByte(), 0xf3.toByte(), 0xe8.toByte())
val NEXTCHAIN_CASH_MAGIC_MAINNET = byteArrayOf(0x72.toByte(), 0x27.toByte(), 0x12.toByte(), 0x21.toByte())

val PROTOCOL_VERSION = 80003
val CLIENT_SUBVERSION = "buwal:0.0.1.0"

val MAX_QUIET_TIME_SEC = 60
val PING_QUIET_TIME_SEC = 35

val BlockchainNetMagic = mapOf<ChainSelector, ByteArray>(ChainSelector.BCHMAINNET to BCH_CASH_MAGIC_MAINNET, ChainSelector.BCHTESTNET to BCH_CASH_MAGIC_TESTNET3, ChainSelector.BCHREGTEST to BCH_CASH_MAGIC_REGTEST, ChainSelector.BCHNOLNET to ByteArray(0), ChainSelector.NEXTCHAIN to NEXTCHAIN_CASH_MAGIC_MAINNET)
val BlockchainPort = mapOf<ChainSelector, Int>(ChainSelector.BCHMAINNET to BCHmainnetPort, ChainSelector.BCHTESTNET to BCHtestnetPort, ChainSelector.BCHREGTEST to BCHregtestPort, ChainSelector.BCHNOLNET to BCHnolnetPort, ChainSelector.NEXTCHAIN to NextChainPort)

open class NetException(msg: String) : Exception(msg)
open class P2PException(msg: String) : NetException(msg)
open class P2PDisconnectedException(msg: String) : P2PException(msg)
open class P2PNoNodesException : P2PException("no nodes")

open class ElectrumNoNodesException : ElectrumException(appI18n(RnoNodes))


open class NetMsgTypeCodingError(msg: String) : P2PException(msg)

enum class NetMsgType
{
    INVALID, VERSION, VERACK, ADDR, INV, GETDATA, MERKLEBLOCK, GETBLOCKS, TX, HEADERS, BLOCK, THINBLOCK, XTHINBLOCK, SENDCMPCT,
    XBLOCKTX, GET_XBLOCKTX, GET_XTHIN, GETADDR, MEMPOOL, PING, PONG, NOTFOUND, FILTERLOAD, FILTERADD, FILTERCLEAR,
    FILTERSIZEXTHIN, FEEFILTER, REJECT, GETHEADERS, SENDHEADERS, XPEDITEDREQUEST, XPEDITEDBLK, XPEDITEDTXN, BUVERSION, BUVERACK, XVERSION, XUPDATE,
    REQ_TXVAL, RES_TXVAL,

    // SV
    PROTOCONF;


    companion object
    {

        fun ba(sz: Int, s: String) = s.toByteArray().copyOf(sz)

        val VERSION_STR = ba(12, "version")
        val VERACK_STR = ba(12, "verack")
        val BUVERSION_STR = ba(12, "buversion")
        val BUVERACK_STR = ba(12, "buverack")
        val XVERSION_STR = ba(12, "xversion")
        val XUPDATE_STR = ba(12, "xversion")
        val SENDCMPCT_STR = ba(12, "sendcmpct")
        val ADDR_STR = ba(12, "addr")
        val INV_STR = ba(12, "inv")
        val GETDATA_STR = ba(12, "getdata")
        val NOTFOUND_STR = ba(12, "notfound")  // response to getdata if some data is not found


        val MERKLEBLOCK_STR = ba(12, "merkleblock")
        val BLOCK_STR = ba(12, "block")

        val FILTERLOAD_STR = ba(12, "filterload")
        val FILTERADD_STR = ba(12, "filteradd")
        val FILTERCLEAR_STR = ba(12, "filterclear")
        val FEEFILTER_STR = ba(12, "feefilter")

        val TX_STR = "tx".toByteArray().copyOf(12)
        val GETADDR_STR = "getaddr".toByteArray().copyOf(12)
        val PING_STR = "ping".toByteArray().copyOf(12)
        val PONG_STR = "pong".toByteArray().copyOf(12)

        val GETHEADERS_STR = ba(12, "getheaders")
        val HEADERS_STR = ba(12, "headers")

        val SENDHEADERS_STR = ba(12, "sendheaders")  // Say that we want block headers, not INV
        val REJECT_STR = ba(12, "reject")
        val MEMPOOL_STR = ba(12, "mempool")

        val REQ_TXVAL_STR = ba(12, "req-txval")
        val RES_TXVAL_STR = ba(12, "res-txval")

        val PROTOCONF_STR = ba(12, "protoconf")

        val INVALID_STR = "".toByteArray().copyOf(12)

        fun cvt(data: ByteArray): NetMsgType
        {
            // version msgs
            if (data contentEquals VERSION_STR) return VERSION
            else if (data contentEquals VERACK_STR) return VERACK
            else if (data contentEquals BUVERSION_STR) return BUVERSION
            else if (data contentEquals BUVERACK_STR) return BUVERACK
            else if (data contentEquals XVERSION_STR) return XVERSION
            else if (data contentEquals XUPDATE_STR) return XUPDATE
            else if (data contentEquals SENDCMPCT_STR) return SENDCMPCT

            // object msgs
            else if (data contentEquals ADDR_STR) return ADDR
            else if (data contentEquals INV_STR) return INV
            else if (data contentEquals TX_STR) return TX
            else if (data contentEquals GETADDR_STR) return GETADDR
            else if (data contentEquals GETDATA_STR) return GETDATA
            else if (data contentEquals PING_STR) return PING
            else if (data contentEquals PONG_STR) return PONG
            else if (data contentEquals GETHEADERS_STR) return GETHEADERS
            else if (data contentEquals HEADERS_STR) return HEADERS
            else if (data contentEquals SENDHEADERS_STR) return SENDHEADERS
            else if (data contentEquals MERKLEBLOCK_STR) return MERKLEBLOCK
            else if (data contentEquals BLOCK_STR) return BLOCK

            // filter msgs
            else if (data contentEquals FILTERLOAD_STR) return FILTERLOAD
            else if (data contentEquals FILTERCLEAR_STR) return FILTERCLEAR
            else if (data contentEquals FILTERADD_STR) return FILTERADD
            else if (data contentEquals FEEFILTER_STR) return FEEFILTER

            // Misc
            else if (data contentEquals NOTFOUND_STR) return NOTFOUND
            else if (data contentEquals REJECT_STR) return REJECT
            else if (data contentEquals MEMPOOL_STR) return MEMPOOL
            else if (data contentEquals REQ_TXVAL_STR) return REQ_TXVAL
            else if (data contentEquals RES_TXVAL_STR) return RES_TXVAL

            // SV
            else if (data contentEquals PROTOCONF_STR) return PROTOCONF
            else throw NetMsgTypeCodingError("converting network bytes " + data.toString(Charset.defaultCharset()) + " to enum")
        }

        fun cvt(data: NetMsgType): ByteArray
        {
            return when (data)
            {
                // version msgs
                VERSION -> VERSION_STR
                VERACK -> VERACK_STR
                BUVERSION -> BUVERSION_STR
                BUVERACK -> BUVERACK_STR
                XVERSION -> XVERSION_STR
                XUPDATE -> XUPDATE_STR
                SENDCMPCT -> SENDCMPCT_STR

                // object msgs
                ADDR -> ADDR_STR
                INV -> INV_STR
                TX -> TX_STR
                GETADDR -> GETADDR_STR
                PING -> PING_STR
                PONG -> PONG_STR
                GETHEADERS -> GETHEADERS_STR
                HEADERS -> HEADERS_STR
                SENDHEADERS -> SENDHEADERS_STR
                GETDATA -> GETDATA_STR
                NOTFOUND -> NOTFOUND_STR

                MERKLEBLOCK -> MERKLEBLOCK_STR
                BLOCK -> BLOCK_STR

                // filter msgs
                FILTERLOAD -> FILTERLOAD_STR
                FILTERADD -> FILTERADD_STR
                FILTERCLEAR -> FILTERCLEAR_STR
                FEEFILTER -> FEEFILTER_STR

                // misc
                REJECT -> REJECT_STR
                MEMPOOL -> MEMPOOL_STR
                RES_TXVAL -> RES_TXVAL_STR
                REQ_TXVAL -> REQ_TXVAL_STR

                // SV
                PROTOCONF -> PROTOCONF_STR
                else ->
                {
                    throw NetMsgTypeCodingError("converting enum " + data.name + " to network bytes"); /* INVALID_STR */
                }
            }
        }

    }

}

/** Helper class to pass around ip and port pairs */
data class IpPort(val ip: String, val port: Int)


class NodeServices
{
    companion object
    {
        val NODE_NETWORK = (1 shl 0)
        val NODE_GETUTXO = (1 shl 1)
        val NODE_BLOOM = (1 shl 2)
        val NODE_WITNESS = (1 shl 3)
        val NODE_XTHIN = (1 shl 4)
        val NODE_BITCOIN_CASH = (1 shl 5)
    }
}

class Inv(var type: Types = Types.ILLEGAL, var id: Guid = Guid()) : BCHserializable()
{
    enum class Types(val v: Int)  // Ordinals must match specification. See protocol.h MSG_ enum
    {
        ILLEGAL(0),
        TX(1),
        BLOCK(2),
        FILTERED_BLOCK(3),
        CMPCT_BLOCK(4),
        XTHIN_BLOCK(5),
        GRAPHENE_BLOCK(6);

        companion object
        {
            private val values = values();
            fun get(value: Int) = values.first { it.v == value }  // throws exception if the int is not in the enum
        }
    }

    // Accept a Hash256 since that's the most common Guid type
    constructor(_type: Types, _id: Hash256) : this(_type, Guid(_id))

    // Serialization
    constructor(buf: BCHserialized) : this()
    {
        val v = buf.deint32()
        type = Types.get(v)
        id = Guid(buf)
    }

    override fun BCHserialize(format: SerializationType): BCHserialized = BCHserialized.int32(type.ordinal, format) + id
    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        type = Types.get(stream.deint32())
        id = Guid(stream)
        return stream
    }
}

class ReceivedMessage(cmd: ByteArray, dat: ByteArray)
{
    val command = cmd
    val data = dat
}

class BCHp2pClient(val chainSelector: ChainSelector, val name: String, val port: Int, val logName: String, val coScope: CoroutineScope, val cocond: CoCond<Boolean>)
{
    companion object
    {
        const val AWAIT_TIME = 100.toLong()
        const val CONNECT_TIMEOUT = 40000  //* How long to attempt to connect in milliseconds
        const val INITIAL_MESSAGE_XCHG_TIMEOUT = 50000 //* how long the initial message exchange may take in milliseconds
    }

    @OptIn(ExperimentalTime::class)
    override fun toString(): String
    {
        val aliveTime = if (beginTime.elapsedNow().inWholeSeconds < 120) beginTime.elapsedNow().toString(DurationUnit.SECONDS) else beginTime.elapsedNow().toString(DurationUnit.MINUTES)
        val lastTime = if (lastReceiveTime.elapsedNow().inWholeSeconds < 120) lastReceiveTime.elapsedNow().toString(DurationUnit.SECONDS) else lastReceiveTime.elapsedNow().toString(DurationUnit.MINUTES)
        return name + ":" + port.toString() + " (alive " + aliveTime + " sent " + bytesSent.toString(Units.BestBytes) + " received " + bytesReceived.toString(Units.BestBytes) + " last " + lastTime + " ago)"
    }

    /** When we first attempted to connect to this peer */
    @ExperimentalTime
    val beginTime: TimeMark = TimeSource.Monotonic.markNow()  // connection happens just after object creation, so marking now is good enough

    /** When we last heard from this peer */
    @ExperimentalTime
    var lastReceiveTime: TimeMark = TimeSource.Monotonic.markNow()  // connection happens just after object creation, so marking now is good enough

    var lastPingCount = 0L
    var pingOutstanding = false

    var bytesSent = 0L
    var bytesReceived = 0L

    val atomicWriter = ThreadCond()
    var sock = Socket() // (ipp, port)
    var inp: InputStream? = null
    var out: OutputStream? = null
    var localServices: Long = (NodeServices.NODE_BITCOIN_CASH).toLong()
    var closed = false

    /** keep track of how often we got a message drop.  If its too many times, drop the node */
    var misbehavingQuietCount = 0
    val MISBEHAVING_QUIET_LIMIT = 25
    var misbehavingBadMessageCount = 0
    val MISBEHAVING_BAD_MESSAGE_LIMIT = 10
    var misbehavingReason: BanReason = BanReason.NOT_BANNED

    var MESSAGE_HEADER_SIZE = 24
    var receivedData = BCHserialized(SerializationType.NETWORK)

    var curMsgCommand = ByteArray(0)
    var curMsgLen: Long = -1
    var curMsgChecksum: Long = 0

    // For messages that pass a nonce to match requests with replies
    var curNonce: Long = 0

    // Version message stuff
    @Volatile
    var version: Int = 0
    var subversion: String = ""
    var services: Long = 0
    var currentHeight: Int = 0
    var timeOffset: Long = 0

    var bloomCount: Int = 0


    //val pendingMerkleBlocks = mutableListOf<MerkleBlock>()

    // We can't really combine these into a dictionary because each has a unique call signature
    var onVersionCallback: MutableList<(BCHp2pClient) -> Unit> = mutableListOf()

    // Callback returns true if this cb should be removed from the callback list.
    // Wants to know about any headers that arrive
    var onHeadersCallback: MutableList<(MutableList<BlockHeader>, BCHp2pClient) -> Boolean> = mutableListOf()

    // For the specific requester
    var headerCallback: ((MutableList<BlockHeader>, BCHp2pClient) -> Boolean)? = null
    val exclusiveHeaders = ThreadCond()  // Only do a single headers request at a time due to P2P protocol limitations

    // Application installed handlers for specific messages
    val onInvCallback: MutableList<(MutableList<Inv>, BCHp2pClient) -> Unit> = mutableListOf()
    val onTxCallback: MutableList<(MutableList<BCHtransaction>, BCHp2pClient) -> Unit> = mutableListOf()
    val onBlockCallback: MutableList<suspend (BCHblock, BCHp2pClient) -> Unit> = mutableListOf()
    val onMerkleBlockCallback: MutableList<(MerkleBlock, BCHp2pClient) -> Unit> = mutableListOf()
    val onAddrCallback: MutableList<(MutableList<PossibleConnection>, BCHp2pClient) -> Unit> = mutableListOf()

    // Since the txValidation message passes a nonce, we can install a handler for a specific response
    // Note application must time out and clean up a request that ignored
    val txValidation: MutableMap<Long, (String) -> Unit> = mutableMapOf()

    var incoming = mutableMapOf<NetMsgType, MutableList<BCHserialized>>()

    var wantsTxRelayed = true

    fun isAlive() = !(closed || sock.isClosed() || sock.isInputShutdown() || sock.isOutputShutdown())

    fun connect(timeout: Int = CONNECT_TIMEOUT): BCHp2pClient
    {
        try
        {
            sock.connect(InetSocketAddress(name, port), timeout)
            inp = sock.getInputStream()
            out = sock.getOutputStream()
            LogIt.info(sourceLoc() + " " + logName + ": Connected. Sending version to ${name}:${port}")
            sendVersion()
        }
        catch (e: P2PDisconnectedException)
        {
            LogIt.warning(sourceLoc() + " " + logName + ": Instant disconnect, likely banned from ${name}:${port}")
            close()
        }
        catch (e: ConnectException)
        {
            LogIt.warning(sourceLoc() + " " + logName + ": Connection not established (${e.toString()}, likely ${name}:${port} has no server")
            close()
            throw e
        }
        catch (e: SocketException)
        {
            LogIt.warning(sourceLoc() + " " + logName + ": Instant disconnect (${e.toString()}, likely banned from ${name}:${port}")
            close()
        }
        return this
    }

    fun close()
    {
        LogIt.warning(sourceLoc() + " " + logName + ": Disconnect, close() was called")
        closed = true
        try
        {
            sock.shutdownInput()
        }
        catch (e: java.io.IOException)
        {
        }  // possibly already shutdown
        try
        {
            sock.shutdownOutput()
        }
        catch (e: java.io.IOException)
        {
        }
        try
        {
            sock.close()
        }
        catch (e: java.io.IOException)
        {
        }
        //inp.close()  // Causes a process crash
        //out.close()
    }

    /** clears all incoming messages */
    fun clear()
    {
        for (v in incoming.values)
        {
            v.clear()
        }
    }

    /** Provide feedback that this node is not responding to our requests */
    fun misbehavingQuiet(howBad: Int = 1)
    {
        misbehavingQuietCount += howBad

        if (misbehavingQuietCount >= MISBEHAVING_QUIET_LIMIT)
        {
            //LogIt.info(sourceLoc() + " " + logName + ": Note, not closing nonresponsive connection")
            close()
        }
    }

    /** Provide feedback that this node is not responding to our requests */
    fun misbehavingBadMessage(howBad: Int = 1)
    {
        misbehavingBadMessageCount += howBad

        if (misbehavingBadMessageCount >= MISBEHAVING_BAD_MESSAGE_LIMIT)
        {
            LogIt.info(sourceLoc() + " " + logName + ": Closing connection -- send bad messages")
            close()
        }
    }

    fun waitForReady()
    {
        var elapsed = 0
        while (version == 0)
        {
            if (closed) throw P2PDisconnectedException(sourceLoc() + " " + logName + ": I closed during initial message exchange")
            else if (!sock.isConnected) throw P2PDisconnectedException(sourceLoc() + " " + logName + ": disconnect during initial message exchange")
            else if (elapsed > INITIAL_MESSAGE_XCHG_TIMEOUT)
            {
                throw P2PDisconnectedException(sourceLoc() + " " + logName + ": too slow during initial message exchange")
            }
            Thread.sleep(50)
            elapsed += 50
        }
        LogIt.info(sourceLoc() + " " + logName + ": Connection ready")

    }

    suspend fun coWaitForReady()
    {
        if (cocond.yield({ version != 0 || !sock.isConnected }, INITIAL_MESSAGE_XCHG_TIMEOUT) == null)
        {
            if (version == 0)
                throw P2PDisconnectedException(sourceLoc() + " " + logName + ": too slow during initial message exchange")
        }

        if (!sock.isConnected)
        {
            throw P2PDisconnectedException(sourceLoc() + " " + logName + ": disconnect during initial message exchange")
        }
    }

    suspend fun waitFor(msg: NetMsgType): BCHserialized
    {
        cocond.yield({ !((incoming[msg] == null || incoming[msg]?.count() == 0) && sock.isConnected) })
        val lst = incoming[msg]
        if (lst != null && lst.count() != 0)
        {
            val msgbytes = lst.get(0)
            lst.removeAt(0)
            return msgbytes
        }


        assert(!sock.isConnected)  // only way to skip the above return
        throw P2PDisconnectedException("disconnect while waiting for message")
    }


    // Helper functions to send various messages

    fun sendTx(tx: ByteArray)
    {
        send(NetMsgType.TX, BCHserialized(tx, SerializationType.NETWORK))
    }

    /** Send a GETADDR request message */
    fun sendGetAddr()
    {
        send(NetMsgType.GETADDR)
    }

    // Send the GETDATA message
    fun sendGetData(invs: List<Inv>)
    {
        if (invs.size > 0)
        {
            val msg = BCHserialized.list(invs, SerializationType.NETWORK)
            send(NetMsgType.GETDATA, msg)
        }
    }

    fun sendGetHeaders(loc: BlockLocator, stopAt: Hash256)
    {
        var msg: BCHserialized = BCHserialized(SerializationType.NETWORK) + loc + stopAt
        send(NetMsgType.GETHEADERS, msg)
    }

    fun sendTxVal(tx: BCHtransaction, nonce: Long? = null, handler: (String) -> Unit)
    {
        val n = nonce ?: curNonce++
        var msg: BCHserialized = BCHserialized(SerializationType.NETWORK) + n + tx.BCHserialize(SerializationType.NETWORK)
        txValidation[n] = handler
        send(NetMsgType.REQ_TXVAL, msg)
    }

    /** Async get block headers.
     * This function only allows one outstanding getheaders per node due to protocol issues.
     * Call @cleanupExclusiveHeaders() if you stop waiting (if cb is never called).
     * Its Ok to unnecessarily call cleanupExclusiveHeaders */
    fun getHeaders(loc: BlockLocator, stopAt: Hash256, cb: (MutableList<BlockHeader>, BCHp2pClient) -> Boolean)
    {
        synchronized(exclusiveHeaders)
        {
            while ((headerCallback != null) && (headerCallback != cb)) exclusiveHeaders.wait()  // Waiting for a prior request from someone else to finish up -- If the request if from "me" (same callback) then allow it to allow retries
            headerCallback = cb
            sendGetHeaders(loc, stopAt)
        }
    }

    /** If you issue a getHeaders() but then give up waiting, call this function */
    fun cleanupExclusiveHeaders(cb: (MutableList<BlockHeader>, BCHp2pClient) -> Boolean)
    {
        synchronized(exclusiveHeaders)
        {
            if (headerCallback == cb)
            {
                headerCallback = null
                exclusiveHeaders.wake()
            }
        }
    }

    // Send a bloom filter that is already serialized as a byte array
    fun sendBloomFilter(filter: ByteArray, inBloomCount: Int = -1)
    {
        if ((inBloomCount == -1) || (bloomCount != inBloomCount))
        {
            if (inBloomCount != -1) bloomCount = inBloomCount
            var msg = BCHserialized(filter, SerializationType.NETWORK)
            send(NetMsgType.FILTERLOAD, msg)
        }
    }

    fun sendVersion()
    {
        // BUIP005 add our special subversion string
        // PushMessage(NetMsgType::VERSION, PROTOCOL_VERSION, nLocalServices, nTime, addrYou, addrMe, nLocalHostNonce,
        //        FormatSubVersion(CLIENT_NAME, CLIENT_VERSION, BUComments), nBestHeight,
        //        !GetBoolArg("-blocksonly", DEFAULT_BLOCKSONLY));

        var remoteServices: Long = NodeServices.NODE_NETWORK.toLong()

        val ip = InetAddress.getByName(name)
        val ipbytes = ip.address.copyOf(16)  // Provide enough room for IPv6 if needed

        val addrYou = sock.inetAddress
        val addrYouBytes = addrYou.address.copyOf(16)  // Provide enough room for IPv6 if needed
        //val portYou = sock.port

        // some const values to send
        val nonce = 1.toLong()
        val sendBlockHeight = 0.toLong()
        val relayTx = false // true // We will turn it on when we install a filter

        var now: Date = java.util.Date() //Instant.now().epochSecond
        var msg =
          BCHserialized(SerializationType.NETWORK) + PROTOCOL_VERSION + localServices + now.time + exactBytes(ipbytes) + localServices + BCHserialized.uint16(port.toLong()) + remoteServices + exactBytes(addrYouBytes) + BCHserialized.uint16(
            port.toLong()) + nonce + CLIENT_SUBVERSION + BCHserialized.uint32(sendBlockHeight) + BCHserialized.boolean(relayTx)

        send(NetMsgType.VERSION, msg)

    }

    //
    /* fun query(command: Int, msg: BCHserialized=BCHserialized()): BCHserialized
    {
        send(command, msg)
        val resp = receive()
    }*/

    fun receive(): Boolean
    {
        var didSomething = false
        val available = try
        {
            inp?.available() ?: 0
        }
        catch (e: IOException)
        {
            LogIt.info(sourceLoc() + ": Disconnecting, IO exception getting available bytes on socket")
            close()
            return false
        }
        if (available == 0) return false

        var dataBuf = ByteArray(available)
        var amtRead = try
        {
            inp?.read(dataBuf) ?: 0 // Its very possible to get fewer bytes than what available() just claimed we had
        }
        catch (e: IOException)
        {
            LogIt.info(sourceLoc() + ": Disconnecting, IO exception reading bytes on socket")
            close()
            return false
        }
        bytesReceived += amtRead
        var data = dataBuf.copyOf(amtRead)  // TODO optimize: this is a waste of a copy just to ignore ending zeros
        // LogIt.info(name + ":" + port.toString() + " Received " + data.size + "(" + curMsgCommand.toString(Charset.defaultCharset()) + ":" + (receivedData.availableBytes() + data.size) + "/" + curMsgLen + " bytes: " + data.ToHex())
        if (data.size > 0)
        {
            receivedData.add(data)
            @OptIn(kotlin.time.ExperimentalTime::class)
            lastReceiveTime = TimeSource.Monotonic.markNow()  // We got something to reset the last receive time
        }

        while (true)
        {
            if ((curMsgLen >= 0L) and (curMsgLen <= receivedData.availableBytes()))
            {
                receivedData.flatten()
                var msg = receivedData.debytes(curMsgLen)
                // todo validate checksum
                //lock.withLock
                run {
                    try
                    {
                        val msgtype = NetMsgType.cvt(curMsgCommand)
                        if (doIHandleThisMessage(msgtype))
                        {
                            var list = incoming.getOrPut(msgtype, { -> mutableListOf() })
                            // LogIt.info(sourceLoc() + " " + logName + ": received message of type " + curMsgCommand.dropLastWhile({ it == 0.toByte() }).toByteArray().toString(Charset.defaultCharset()) + " hex: " + msg.toHex())
                            list.add(BCHserialized(msg, SerializationType.NETWORK))
                            cocond.wake(true)
                        }
                        if (msgtype == NetMsgType.PONG)  // Handle this easy msg inline
                        {
                            pingOutstanding = false
                        }
                    }
                    catch (e: NetMsgTypeCodingError)
                    {
                        LogIt.warning(e.toString())
                        if (DEBUG) throw e  // If in debug mode let's call attention to this bad message, otherwise ignore it
                    }
                }
                curMsgLen = -1
                curMsgCommand.fill(0)
                didSomething = true

            }
            else if ((curMsgLen == -1L) and (receivedData.availableBytes() >= MESSAGE_HEADER_SIZE))
            {
                receivedData.flatten()
                var msgStart = receivedData.debytes(4)
                if (!msgStart.contentEquals(BlockchainNetMagic[chainSelector]!!))
                {
                    LogIt.info(sourceLoc() + " " + logName + ": Invalid message start. Disconnecting")
                    close()
                    return false
                }
                curMsgCommand = receivedData.debytes(12)
                curMsgLen = receivedData.deuint32()
                curMsgChecksum = receivedData.deuint32()
                didSomething = true
                // LogIt.info(name + ":" + port.toString() + " Initiate msg " + curMsgCommand.toString(Charset.defaultCharset()) + " " + (receivedData.availableBytes()) + "/" + curMsgLen)
            }
            else break  // nothing to do so no need to loop
        }

        return didSomething
    }

    fun processVersionMessage(msg: BCHserialized)
    {
        var curTime = System.currentTimeMillis() / 1000
        msg.flatten()
        val ver = msg.deint32()
        services = msg.deuint64()
        var time = msg.deuint64()
        timeOffset = curTime - time

        // AddrYou
        var services2 = msg.deuint64()
        var ip = msg.debytes(16)
        var port = msg.deuint16()  // TODO ntohs
        LogIt.info(sourceLoc() + " " + logName + ": received version info.  Services: " + services2.toString(16) + "  IP: " + ip.toHex() + "  Port: " + port)

        if (msg.availableBytes() > 0)
        {
            // AddrMe
            var services3 = msg.deuint64()
            var ipFrom = msg.debytes(16)
            var portFrom = msg.deuint16()
            var nonce = msg.deuint64()
            LogIt.info(sourceLoc() + " " + logName + ": my reflected info. " + services3.toString(16) + "  IP: " + ipFrom.toHex() + "  Port: " + portFrom + "  Nonce: " + nonce)
        }
        if (msg.availableBytes() > 0)
        {
            subversion = msg.deString()
        }
        if (msg.availableBytes() > 0)
        {
            currentHeight = msg.deint32()
        }
        if (msg.availableBytes() > 0)
        {
            wantsTxRelayed = msg.deboolean()
        }



        version = ver  // set this last because blocking Ready depends on it
        cocond.wake(true)

        for (fn in onVersionCallback)
            fn(this)
    }

    fun processHeadersMessage(msg: BCHserialized)
    {
        val hdrs: MutableList<BlockHeader> = msg.delist({ strm -> BlockHeader(strm) })

        synchronized(exclusiveHeaders)
        {
            val hdrs2 = hdrs.toMutableList()  // Make a copy to avoid concurrent mod exception in this callback and the next
            headerCallback?.invoke(hdrs2, this)
            headerCallback = null
            exclusiveHeaders.wake()
        }

        for (fn in onHeadersCallback)
            coScope.launch { fn(hdrs, this@BCHp2pClient) }
    }

    @Suppress("UNUSED_PARAMETER")
    fun processGetDataMessage(msg: BCHserialized)
    {
        // nothing to do, this code doesn't implement the server side yet or probably ever
    }

    fun processInvMessage(msg: BCHserialized)
    {
        val invs: MutableList<Inv> = msg.delist({ strm -> Inv(strm) })
        // LogIt.info(sourceLoc() + " " + name + ": Received INV")
        for (fn in onInvCallback) coScope.launch { fn(invs, this@BCHp2pClient) }
    }

    fun processTxMessage(msg: BCHserialized)
    {
        val tx: BCHtransaction = BCHtransaction(chainSelector, msg)
        // LogIt.info(sourceLoc() + " " + name + ": TX arrived ${tx.hash.toHex()}")
        for (fn in onTxCallback) coScope.launch { fn(mutableListOf(tx), this@BCHp2pClient) }
    }

    fun processBlockMessage(msg: BCHserialized)
    {
        val blk: BCHblock = BCHblock(chainSelector, msg)
        for (fn in onBlockCallback) coScope.launch { fn(blk, this@BCHp2pClient) }
    }

    fun processMerkleBlockMessage(msg: BCHserialized)
    {
        val blk = MerkleBlock(chainSelector, msg)
        // LogIt.info("Processing merkle block ${blk.hash.toHex()}")
        for (fn in onMerkleBlockCallback) coScope.launch { fn(blk, this@BCHp2pClient) }
    }

    fun processAddrMessage(msg: BCHserialized)
    {
        val netPeers: MutableList<PossibleConnection> = msg.delist({ strm -> PossibleConnection(strm) });
        LogIt.info("Peer addresses received: " + netPeers.size)
        for (fn in onAddrCallback) coScope.launch { fn(netPeers, this@BCHp2pClient) }
    }

    fun processTxValResponse(msg: BCHserialized)
    {
        val nonce: Long = msg.deuint64()
        LogIt.info("tx validation response received: " + nonce)
        val requestCallback = txValidation[nonce]
        if (requestCallback != null)
        {
            txValidation.remove(nonce)
            val result = msg.deString()
            requestCallback(result)
        }
    }


    suspend fun coProcessForever()
    {
        while (!sock.isInputShutdown())
        {
            if (!process()) delay(20)
        }
    }

    fun processForever()
    {
        while (!sock.isInputShutdown())
        {
            if (!process()) Thread.sleep(20)
        }
    }

    // Return true for all messages that I handle
    fun doIHandleThisMessage(msgType: NetMsgType): Boolean
    {
        if ((msgType == NetMsgType.VERSION) || (msgType == NetMsgType.BUVERSION) ||
          (msgType == NetMsgType.PING) || (msgType == NetMsgType.ADDR) ||
          (msgType == NetMsgType.PROTOCONF) || (msgType == NetMsgType.HEADERS) ||
          (msgType == NetMsgType.INV) || (msgType == NetMsgType.BLOCK) ||
          (msgType == NetMsgType.MERKLEBLOCK) || (msgType == NetMsgType.NOTFOUND) ||
          (msgType == NetMsgType.REJECT) || (msgType == NetMsgType.TX)
          || (msgType == NetMsgType.RES_TXVAL))
            return true
        return false
    }

    // Process a message
    @Synchronized
    fun process(): Boolean
    {
        var didSomething = false
        try
        {
            didSomething = receive()

            // Note if you add a handler for a message here you must add it to doIHandleThisMessage()!

            // If I got a version message, autorespond with VERACK
            var list = incoming[NetMsgType.VERSION]
            list?.let {
                if (it.size > 0)
                {
                    send(NetMsgType.VERACK)
                    processVersionMessage(it[0])
                    it.clear() // so I don't verack multiple times per version
                    didSomething = true
                    sendGetAddr()  // for any new connection, let's get who it knows about
                }
            }

            list = incoming[NetMsgType.BUVERSION]
            list?.let {
                if (it.size > 0)
                {
                    send(NetMsgType.BUVERACK, BCHserialized.uint16(0, SerializationType.NETWORK))  // optionally send your port
                    it.clear() // so I only buverack once
                    didSomething = true
                }
            }

            list = incoming[NetMsgType.PING]
            list?.let({
                if (it.size > 0)
                {
                    var msg = it[0]
                    send(NetMsgType.PONG, msg)  // send the nonce back
                    it.removeAt(0)
                    didSomething = true
                }
            })

            list = incoming[NetMsgType.ADDR]
            list?.let {
                if (it.size > 0)
                {
                    var msg = it.removeAt(0)
                    processAddrMessage(msg)
                    didSomething = true
                }


            }

            list = incoming[NetMsgType.RES_TXVAL]
            list?.let {
                if (it.size > 0)
                {
                    var msg = it.removeAt(0)
                    processTxValResponse(msg)
                    didSomething = true
                }


            }

            list = incoming[NetMsgType.PROTOCONF]
            list?.let {
                if (it.size > 0)
                {
                    LogIt.info("Disconnecting: connected to SV node")
                    misbehavingReason = BanReason.INCORRECT_BLOCKCHAIN
                    close()
                }


            }
            // If someone has installed a headers message handler, then consume headers messages
            if (true) // onHeadersCallback.size > 0)
            {
                var hdrlst = incoming[NetMsgType.HEADERS]
                hdrlst?.let {
                    if (it.size > 0)
                    {
                        var msg = it.removeAt(0)
                        processHeadersMessage(msg)
                        didSomething = true
                    }
                }
            }
            // If someone has installed an inv message handler, then consume headers messages
            if (onInvCallback.size > 0)
            {
                var hdrlst = incoming[NetMsgType.INV]
                hdrlst?.let {
                    while (it.size > 0)
                    {
                        var msg = it.removeAt(0)
                        processInvMessage(msg)
                        didSomething = true
                    }
                }
            }

            // If someone has installed a BLOCK message handler, then consume incoming transactions
            if (onBlockCallback.size > 0)
            {
                var hdrlst = incoming[NetMsgType.BLOCK]
                hdrlst?.let {
                    if (it.size > 0)
                    {
                        var msg = it.removeAt(0)
                        processBlockMessage(msg)
                        didSomething = true
                    }
                }
            }
            // If someone has installed a MERKLEBLOCK message handler, then consume incoming transactions
            if (onMerkleBlockCallback.size > 0)
            {
                val hdrlst = incoming[NetMsgType.MERKLEBLOCK]
                while ((hdrlst != null) && (hdrlst.size > 0))  // The purpose of this while loop is to process every merkleblock before processing any transactions
                {
                    var msg = hdrlst.removeAt(0)
                    processMerkleBlockMessage(msg)
                    didSomething = true
                }
            }

            // Someone could be lying to us about a block or tx being NOTFOUND so drop these messages for now.  However, in the future it may make sense to handle multiple notfound from different sources
            val nflst = incoming[NetMsgType.NOTFOUND]
            if (nflst != null) for (nf in nflst)
            {
                LogIt.warning("received NOTFOUND message. Dropping it")
            }
            incoming[NetMsgType.NOTFOUND]?.clear()

            val errlst = incoming[NetMsgType.REJECT]
            while ((errlst != null) && (errlst.size > 0))  // The purpose of this while loop is to process every merkleblock before processing any transactions
            {
                val MAX_REJECT_MESSAGE_LENGTH: Long = 111
                var msg = errlst.removeAt(0)
                val rejectedCommand = msg.deString()
                val rejectCode = msg.deuint8()
                val rejectStr = msg.deString()
                LogIt.warning(sourceLoc() + logName + ": Received reject of ${rejectedCommand}, code: ${rejectCode}, reason: ${rejectStr}")
                didSomething = true
            }

            // If someone has installed a TX message handler, then consume incoming transactions
            // Note that the tx handler must happen after the merkle block handler so that any transactions that arrive after a merkle block arrives are processed in that order
            if (onTxCallback.size > 0)
            {
                var hdrlst = incoming[NetMsgType.TX]
                hdrlst?.let {
                    while (it.size > 0)
                    {
                        var msg = it.removeAt(0)
                        processTxMessage(msg)
                        didSomething = true
                    }
                }
            }

            // TODO periodically clean up anything that hasn't been processed
        }
        catch (e: P2PDisconnectedException)
        {
            LogIt.info(sourceLoc() + "Disconnecting: " + e.message)
            close()  // once I close my side higher layer software will detect this and reconnect using a different object
        }

        return didSomething
    }


    /*
    // Ask this node for a list of peers
    fun queryPeers():BCHserialized
    {
        sendPeerRequest()
        var list = incoming.getOrPut(NetMsgType.ADDR, { -> mutableListOf() })
        while (list.size == 0)
        {
            receive()
        }
        return list.removeAt(0)

    }

    fun sendPeerRequest() = send(NetMsgType.GETADDR, BCHserialized())
     */


    fun sendInv(invs: MutableList<Inv>) = send(NetMsgType.INV, BCHserialized(SerializationType.NETWORK) + invs)

    fun send(command: ByteArray, msg: BCHserialized) = send(command, msg.flatten())
    fun send(command: NetMsgType, msg: BCHserialized = BCHserialized(SerializationType.NETWORK)) = send(NetMsgType.cvt(command), msg.flatten())


    fun send(command: ByteArray, msg: ByteArray)
    {
        // LogIt.info(sourceLoc() + " " + logName + ": Send " + command.dropLastWhile({ it == 0.toByte() }).toByteArray().toString(Charset.defaultCharset()))

        // example of debugging to trigger on a specific message type
        //if (command.dropLastWhile({ it == 0.toByte() }).toByteArray().toString(Charset.defaultCharset()) == "getheaders")
        //{
        //    LogIt.info("DBG: getheader")
        //}
        var msghash = Hash.hash256(msg)
        // without libbitcoincash.so:
        //var msghash = MessageDigest.getInstance("SHA-256").digest(msg)
        //msghash = MessageDigest.getInstance("SHA-256").digest(msghash)

        val msglen = BCHserialized.uint32(msg.size.toLong(), SerializationType.NETWORK).flatten()
        /*
        var buf = MutableList<ByteArray>(5,
                { i ->
                    when
                    {
                        i == 0 -> BlockchainNetMagic[chainSelector] ?: BCH_CASH_MAGIC_MAINNET
                        i == 1 -> command
                        i == 2 -> msglen
                        i == 3 -> msghash.sliceArray(IntRange(0, 3))
                        i == 4 -> msg

                        else -> ByteArray(0, { _ -> 0 })
                    }
                }).join()
                */

        val buf = mutableListOf(BlockchainNetMagic[chainSelector] ?: BCH_CASH_MAGIC_MAINNET, command, msglen, msghash.sliceArray(IntRange(0, 3)), msg).join()

        try
        {
            synchronized(atomicWriter)
            {
                val tmp = out
                if (tmp != null)
                {
                    tmp.write(buf)
                    tmp.flush()
                    bytesSent += buf.size
                }
            }
        }
        catch (e: SocketException)  // Happens if other side disconnects
        {
            close()
            LogIt.warning(sourceLoc() + ": Disconnecting, other side dropped")
            throw P2PDisconnectedException("disconnect during initial message exchange")
        }
    }
}

/** Indicate why we chose not to connect to a node */
enum class BanReason
{
    NOT_BANNED,
    TOO_SLOW,
    EARLY_DISCONNECT,
    CANNOT_CONNECT,  // Server is probably down, wrong port, or never existed
    BAD_DATA,
    INCORRECT_BLOCKCHAIN
}

/** Data related to why we banned a node */
@OptIn(ExperimentalTime::class)
class BanData(val reason: BanReason)
{
    val start = TimeSource.Monotonic.markNow()
}


//? The CnxnMgr handles discovery, connection, and maintenance of all connections into the Peer-2-Peer network
//  This allows higher layers to treat peers as a grab bag of nodes, ignoring issues like reconnecting, periodically
//  cycling connections, and searching for new nodes.
@cli(Display.Simple, "Handles connections to the blockchain network")
abstract class CnxnMgr
{
    /** returns the blockchain's name (for logging/debug) */
    @cli(Display.Simple, "blockchain name")
    abstract val chainName: String

    /** Returns a random connection from the connection manager's node set */
    abstract suspend fun getp2p(): BCHp2pClient

    /** Returns a random connection from the connection manager's node set */
    @cli(Display.Simple, "Get a p2p connection")
    abstract fun getNode(block: Boolean = true): BCHp2pClient

    /** Returns a random connection from the connection manager's node set, but not one of these.
     * Returns null if no connections left */
    abstract fun getAnotherNode(notThese: Set<BCHp2pClient>): BCHp2pClient?

    /** Returns a random connection from the connection manager's node set */
    @cli(Display.Simple, "Get an electrum connection")
    abstract fun getElectrum(): ElectrumClient

    /** Returns a random connection from the connection manager's node set, but not one of these.
     * Returns null if no connections left */
    abstract fun getAnotherElectrum(notThese: Set<ElectrumClient>): ElectrumClient?

    /** Returns a random connection from the connection manager's node set, but not one of these.
     * Returns null if no connections left */
    //abstract suspend fun coGetAnotherNode(notThese: Set<BCHp2pClient>): BCHp2pClient?

    /** Returns the number of currently connected peers */
    @cli(Display.Simple, "Return the number of connected peers")
    @Deprecated("use .size")
    abstract fun numPeers(): Int

    /** Returns the number of currently connected peers */
    @cli(Display.Simple, "Return the number of connected peers")
    abstract val size: Int

    // It is better to use the connection getter functions, rather than accessing this list.
    // This is meant for display, health and statistics, rather than access
    @cli(Display.Simple, "Return list of connected peers")
    abstract val p2pCnxns: List<BCHp2pClient>

    // It is better to use the connection getter functions, rather than accessing this list.
    // This is meant for display, health and statistics, rather than access
    @cli(Display.Simple, "Return list of connected peers")
    abstract val electrumCnxns: List<ElectrumClient>

    /** Returns a list of results of your function applied to each connection in this list.
     * Return null to not include this connection in the list.
     * Note that the underlying data structure is synchronized so your function is called with this lock taken */
    abstract fun <T> mapConnections(mapfilter: (BCHp2pClient) -> T?): List<T>

    /** Returns true if we are connected to this node/port */
    abstract fun connectedTo(ip: String, port: Int): Boolean

    /** wake up and analyze the health of connections, especially the optional passed connection */
    abstract suspend fun coReport(problemCnxn: BCHp2pClient? = null)

    /** wake up and analyze the health of connections, especially the optional passed connection */
    abstract fun report(problemCnxn: BCHp2pClient? = null)

    /** Add a potential node into the list of possible connections
     * @param ip IP address or domain name of the node
     * @param port TCP port of the node
     */
    @cli(Display.Simple, "Add a potential p2p node")
    abstract fun add(ip: String, port: Int)

    /** Exclusively connect to a single node.  All other existing connections will be dropped
     * @param ip IP address or domain name of the node, or null to remove any exclusive node
     * @param port TCP port of the node
     */
    @cli(Display.Simple, "Exclusively connect to this node")
    abstract fun exclusiveNode(ip: String?, port: Int)

    /** Prefer a particular node.  An existing connections may be dropped to make room
     * @param ip IP address or domain name of the node, or null to remove any exclusive node
     * @param port TCP port of the node
     */
    @cli(Display.Simple, "Prefer this node")
    abstract fun preferNode(ip: String?, port: Int)

    /** Start connection manager operation */
    abstract fun start()

    /** Restart connection manager operation after suspend may have killed threads */
    abstract fun restart()

    /** Stop connection manager operation */
    abstract fun stop()

    /** clear all incoming messages, drop all connections */
    abstract fun clear()

    /** clear all bans (if param is null) or clear any ban of the specified IP address */
    @cli(Display.Simple, "Clear all banned nodes")
    abstract fun clearBanned(ip: String? = null)


    /** tell all nodes, including future connections, that we are only interested in things that match this bloom filter.
     * To enable multiple wallets to use the same Blockchain, do NOT use this API directly.  Instead use the blockchain API */
    abstract fun setAddressFilter(bloom: ByteArray, onBloomInstalled: (() -> Unit)?)

    // Add handler callbacks for incoming data
    // The connection manager will try to (but not guarantee) eliminate duplicates

    /** Add a callback to handle incoming transactions */
    abstract fun addTxHandler(cb: (MutableList<BCHtransaction>) -> Unit)

    /** Add a callback to handle incoming blocks -- downcast the parameter to either BCHBlock or MerkleBlock */
    abstract fun addBlockHandler(cb: suspend (MutableList<BlockHeader>) -> Unit)

    /** Add a callback to handle incoming incomplete MerkleBlocks */
    abstract fun addPartialBlockHandler(cb: suspend (MutableList<MerkleBlock>) -> Unit)

    /** Add a callback to handle incoming inventory (right now, only blocks, the CnxnMgr auto-handles tx INVs) */
    abstract fun addInvHandler(cb: suspend (MutableList<Inv>) -> Unit)

    /** Add a callback to handle incoming block headers */
    abstract fun addBlockHeadersHandler(cb: (MutableList<BlockHeader>) -> Unit)

    /** Send a transaction to one node */
    @cli(Display.Simple, "Send transaction to one (random) node")
    abstract suspend fun sendTransaction(tx: ByteArray)

    /* Broadcast a transaction to all connected nodes */
    @cli(Display.Simple, "Send transaction to all connected nodes")
    abstract fun broadcastTransaction(tx: ByteArray)

    /** Broadcast a set of transactions to the network */
    abstract fun sendTransactions(txes: List<ByteArray>)

    /** report a node as having a problem */
    abstract fun misbehaving(cnxn: BCHp2pClient, reason: BanReason, count: Int = 100)

    /** If the connection state changes, this function will be called */
    var changeCallback: ((CnxnMgr, BCHp2pClient) -> Unit)? = null


    /** Derived classes must override and implement their continuous connection management.  This function does not return until stop() is called.
     * Do not call directly -- called by start() */
    abstract fun run()
}


abstract class CommonCnxnMgr : CnxnMgr()
{
    var processingThread: Thread? = null

    protected val coCtxt: CoroutineContext = Executors.newFixedThreadPool(4).asCoroutineDispatcher()
    protected val coScope: CoroutineScope = kotlinx.coroutines.CoroutineScope(coCtxt)
    protected val cocond = CoCond<Boolean>(coScope)

    val wakingDelay = ThreadCond()
    val OUT_OF_NODES_DELAY = 200.toLong()
    val SIMULTANEOUS_CNXN_INIT = 5  //?< Try this many initial connection attempts simultaneously

    /** True if the connection manager should stop operating & clean up any threads */
    var done = false

    /** Tell peers to only be interested in things that match this bloom */
    var addressBloomFilter: ByteArray? = null

    /** If the bloom filter changes, this is bumped so we know that it has changed and should send it to peers */
    var bloomCount = 0

    /** If a message of a certain type comes in, call this callback with the message for processing */
    var txCallback: ((MutableList<BCHtransaction>) -> Unit)? = null

    /** a block has arrived.  This could be a BCHBlock or a MerkleBlock */
    var blockCallback: (suspend (MutableList<BlockHeader>) -> Unit)? = null

    /** A partially complete MerkleBlock arrived */
    var partialBlockCallback: (suspend (MutableList<MerkleBlock>) -> Unit)? = null

    /** a block header has arrived */
    var headersCallback: ((MutableList<BlockHeader>) -> Unit)? = null

    /** a notification of an object's existence has arrived */
    var invCallback: (suspend (MutableList<Inv>) -> Unit)? = null

    /** All current connections */
    val cnxnLock = ThreadCond()
    var cnxns: MutableList<BCHp2pClient> = mutableListOf()
    var ecnxns: MutableList<ElectrumClient> = mutableListOf()

    /* offer these connection lists to the API */
    override val p2pCnxns: List<BCHp2pClient>
        get()
        {
            synchronized(cnxnLock)
            {
                return cnxns.toList()
            }
        }

    override val electrumCnxns: List<ElectrumClient>
        get()
        {
            synchronized(cnxnLock)
            {
                return ecnxns.toList()
            }
        }


    /** if this is not null, try all communications to this connection first */
    var preferredCnxn: BCHp2pClient? = null

    /** Don't reconnect to these nodes */
    val bannedLock = ThreadCond()
    val bannedNodes: MutableMap<String, BanData> = mutableMapOf()

    /** used to offer a different node each time to callers */
    protected var cnxnRoundRobin = 0

    /** supply electrum server ip and ports if we need them */
    var getElectrumServerCandidate: ((ChainSelector) -> IpPort)? = null


    /** Returns a random connection from the connection manager's node set */
    override fun getElectrum(): ElectrumClient
    {
        throw ElectrumNoNodesException()
    }

    /** Returns a random connection from the connection manager's node set, but not one of these.
     * Returns null if no connections left */
    override fun getAnotherElectrum(notThese: Set<ElectrumClient>): ElectrumClient?
    {
        throw ElectrumNoNodesException()
    }

    /** Returns a list of connected peers.  This API works simultaneously as map (transformation) and filter functions and calls the passed function on every item in the connection list.
     * @param mapfilter: Return T to add it to the returned list, return null skip
     * @return List of whatever your transformation/filter function returned, skipping null */
    override fun <T> mapConnections(mapfilter: (BCHp2pClient) -> T?): List<T>
    {
        val ret = mutableListOf<T>()
        synchronized(cnxnLock)
        {
            for (c in cnxns)
            {
                mapfilter(c)?.let { ret.add(it) }
            }
        }
        return ret
    }

    /** remove a node (or all nodes if ip==null) from the ban table */
    override fun clearBanned(ip: String?)
    {
        if (ip == null) bannedNodes.clear()
        else bannedNodes.remove(ip)
    }

    override fun clear()
    {
        LogIt.warning(sourceLoc() + ": Disconnecting everything.")
        synchronized(cnxnLock)
        {
            for (c in cnxns)
                c.close()
            for (c in cnxns)
                c.clear()
            cnxns.clear()
        }
    }

    /** Returns true if we are connected to this node/port */
    override fun connectedTo(ip: String, port: Int): Boolean
    {
        synchronized(cnxnLock)
        {
            for (c in cnxns)
            {
                if ((c.port == port) && (c.name == ip)) return true
            }
            return false
        }
    }

    /** Start the connection manager processing thread */
    override fun start()
    {
        restart()
    }

    override fun restart()
    {
        if ((processingThread == null) || (processingThread?.state == Thread.State.TERMINATED))
        {
            processingThread = thread(true, true, null, chainName + "_cnxnMgr") { run() }
        }
    }

    /** Stop the connection manager processing thread asynchronously */
    override fun stop()
    {
        done = true
    }

    /** wake up and analyze my connections, especially the optional passed connection */
    override suspend fun coReport(problemCnxn: BCHp2pClient?)
    {
        //cnxnEvent.send(problemCnxn)
        wakingDelay.wake()
    }

    /** wake up and analyze my connections, especially the optional passed connection */
    override fun report(problemCnxn: BCHp2pClient?)
    {
        wakingDelay.wake()
    }

    override fun numPeers(): Int = synchronized(cnxnLock) { cnxns.size.toInt() }
    override val size: Int
      get() = cnxns.size.toInt()

    override fun sendTransactions(txes: List<ByteArray>)
    {
        val node = getNode()

        for (tx in txes)
        {
            val txid = Hash256(Hash.hash256(tx))
            LogIt.info(sourceLoc() + " " + node.logName + " " + "Sending tx: " + txid.toHex())
            LogIt.info(" Raw Tx:")
            val hex = tx.toHex();
            // DEBUG
            val decodedTx = BCHtransaction(ChainSelector.BCHTESTNET, BCHserialized(tx, SerializationType.NETWORK))
            decodedTx.debugDump()
            for (c in hex.chunked(900))
            {
                LogIt.info(c)
            }

            if (tx.size < 100)  // TODO use consensus param
            {
                throw TransactionException("Transaction is too small at ${tx.size} bytes")
            }
            node.sendTx(tx)
        }
    }

    override suspend fun sendTransaction(tx: ByteArray)
    {
        val node = getNode()

        LogIt.info(sourceLoc() + " " + node.logName + " " + "Sending tx: " + Hash256(Hash.hash256(tx)).toHex() + " Raw: " + tx.toHex())
        if (tx.size < 100)  // TODO use consensus param
        {
            throw TransactionException("Transaction is too small at ${tx.size} bytes")
        }
        node.sendTx(tx)
    }

    override fun broadcastTransaction(tx: ByteArray)
    {
        if (tx.size < 100)  // TODO use consensus param
        {
            throw TransactionException("Transaction is too small at ${tx.size} bytes")
        }
        synchronized(cnxnLock)
        {
            if (cnxns.size == 0)
            {
                LogIt.warning(sourceLoc() + " " + chainName + ": No available connections to broadcast tx")
            }
            else
            {
                for (c in cnxns)
                {
                    try
                    {
                        if (c.isAlive()) c.sendTx(tx)
                    }
                    catch (e: P2PException)  // Its possible that the connection is bad at that moment
                    {
                        LogIt.warning(e.toString())
                    }
                }
            }
        }
    }

    override fun addTxHandler(cb: (MutableList<BCHtransaction>) -> Unit)
    {
        txCallback = cb
    }

    /** Add a callback to handle incoming incomplete MerkleBlocks */
    override fun addPartialBlockHandler(cb: suspend (MutableList<MerkleBlock>) -> Unit)
    {
        partialBlockCallback = cb
    }

    override fun addBlockHandler(cb: suspend (MutableList<BlockHeader>) -> Unit)
    {
        blockCallback = cb
    }

    override fun addInvHandler(cb: suspend (MutableList<Inv>) -> Unit)
    {
        invCallback = cb
    }

    override fun addBlockHeadersHandler(cb: (MutableList<BlockHeader>) -> Unit)
    {
        headersCallback = cb
    }

    override fun setAddressFilter(bloom: ByteArray, onBloomInstalled: (() -> Unit)?)
    {
        addressBloomFilter = bloom
        bloomCount += 1
        synchronized(cnxnLock)
        {
            if (cnxns.size == 0)
                onBloomInstalled?.invoke()
            else
            {
                val cnxnSnap = cnxns.toList()
                val can = CallAfterN(onBloomInstalled, cnxnSnap.size)
                for (c in cnxnSnap)
                {
                    launch {
                        try
                        {
                            c.sendBloomFilter(bloom, bloomCount)
                            delay(500)  // give time for the filter to get to the node, since the protocol does not ACK messages, there's not much else we can do
                        }
                        catch (e: P2PDisconnectedException)
                        {
                        }
                        can() // If we disconnect, then we still need to indicate that the filter is installed, because the idea is to wait to call this until its been installed on all CONNECTED nodes
                    }
                }
            }
        }
    }


    suspend fun processInvMessage(invs: MutableList<Inv>, node: BCHp2pClient)
    {
        // for (fn in onHeadersCallback) fn(hdrs, node)
        val txes: MutableList<Inv> = mutableListOf()
        val blocks: MutableList<Inv> = mutableListOf()
        for (inv in invs)
        {
            // LogIt.info(sourceLoc() + " " + node.logName + ": received INV for " + inv.type + " " + inv.id.toHex())

            if (inv.type == Inv.Types.TX) txes.add(inv)
            if (inv.type == Inv.Types.BLOCK)
            {
                blocks.add(inv)
            }
        }

        try
        {
            if (txes.size > 0)
                node.sendGetData(txes)
            if (blocks.size > 0)
                invCallback?.invoke(blocks)
        }
        catch (e: P2PDisconnectedException)
        {
            LogIt.info("Disconnect while requesting data from INV")
        }
    }

    fun processTxMessage(txs: MutableList<BCHtransaction>, node: BCHp2pClient)
    {
        for (tx in txs)
        {
            LogIt.info(sourceLoc() + " " + node.logName + ": received TX " + tx.hash.toHex() + " contents: " + tx.toHex())
        }
        assert(txCallback != null)
        txCallback?.let { it(txs) }
    }

    @Suppress("UNUSED_PARAMETER")
    fun processBlockMessage(blocks: MutableList<BCHblock>, node: BCHp2pClient)
    {
        var blksAsHeaders: MutableList<BlockHeader> = mutableListOf()
        for (blk in blocks)
        {
            LogIt.info(sourceLoc() + " " + node.logName + ": received block " + blk.hash.toHex())
            blksAsHeaders.add(blk)
        }
        blockCallback?.let { launch { it(blksAsHeaders) } }
    }

    @Suppress("UNUSED_PARAMETER")
    fun processMerkleBlockMessage(blocks: MutableList<MerkleBlock>, node: BCHp2pClient)
    {
        for (m in blocks)
        {
            if ((m.txes.size > 0) or (m.txHashes.size > 0))  // Only log something if the block is interesting to me
                LogIt.info(sourceLoc() + " " + node.logName + ": Received MerkleBlock ${m.hash.toHex()} with ${m.numTx} tx containing ${m.txes} full tx and ${m.txHashes} hashes")
        }
        val readyBlksAsHeaders: MutableList<BlockHeader> = mutableListOf()
        val unreadyBlks: MutableList<MerkleBlock> = mutableListOf()
        for (blk in blocks)
        {
            if (blk.txHashes.size == 0)  // If we have all the tx (generally this happens when there are no interesting txes) then go ahead and pass the merkle block onwards now
                readyBlksAsHeaders.add(blk)
            else
                unreadyBlks.add(blk)
        }

        if (readyBlksAsHeaders.size > 0) blockCallback?.let { launch { it(readyBlksAsHeaders) } }
        if (unreadyBlks.size > 0) partialBlockCallback?.let { launch { it(unreadyBlks) } }

    }

    @Suppress("UNUSED_PARAMETER")
    fun processHeaderMessage(hdrs: MutableList<BlockHeader>, node: BCHp2pClient)
    {
        headersCallback?.invoke(hdrs)
    }

    /** Loops looking for an alive connection, returns null if no connections are left  */
    fun grabAndCheckConnection(): BCHp2pClient?
    {
        while (true)
        {
            synchronized(cnxnLock)
            {
                if (cnxns.size == 0) return null
                val idx = cnxnRoundRobin % cnxns.size
                val cnxn = cnxns[idx]
                cnxnRoundRobin += 1

                if (!cnxn.isAlive())
                {
                    LogIt.warning(sourceLoc() + " " + cnxn.logName + ": Disconnecting, connection isn't alive")
                    cnxn.close()
                    cnxns.removeAt(idx)
                    report()
                }
                else
                {
                    return cnxn
                }
            }
        }

    }

    override fun getNode(block: Boolean): BCHp2pClient
    {
        while (true)
        {

            if ((synchronized(cnxnLock) { cnxns.size }) > 0)
            {
                val c = grabAndCheckConnection()
                if (c != null) return c
                if (block == false) throw P2PNoNodesException()
            }
            else if (!block) throw P2PNoNodesException()
            else wakingDelay.delay(OUT_OF_NODES_DELAY)
        }
    }

    // Returns a random connection from my list
    override suspend fun getp2p(): BCHp2pClient
    {
        while (true)
        {
            preferredCnxn?.let { if (it.isAlive()) return it }
            if ((synchronized(cnxnLock) { cnxns.size }) > 0)
            {
                val c = grabAndCheckConnection()
                if (c != null) return c
            }
            else wakingDelay.delay(OUT_OF_NODES_DELAY)
        }
    }

    /** Returns a random connection from the connection manager's node set, but not one of these.
     * Returns null if no connections left */
    override fun getAnotherNode(notThese: Set<BCHp2pClient>): BCHp2pClient?
    {
        var tries = 0
        while (true)
        {
            tries += 1
            preferredCnxn?.let { if (it.isAlive() && !notThese.contains(it)) return it }
            val csize = synchronized(cnxnLock) { cnxns.size }
            if (tries > csize) return null // I've tried everything in cnxns
            if (csize > 0)
            {
                val c = grabAndCheckConnection()
                if (c != null)
                {
                    if (!notThese.contains(c)) return c // don't give it multiple times
                }
            }
            else wakingDelay.delay(OUT_OF_NODES_DELAY)
        }
    }
}

/** This class tracks possible blockchain nodes -- we have learned about this node from some source but the data may not be accurate */
@kotlin.ExperimentalUnsignedTypes
class PossibleConnection() : BCHserializable()
{
    // This matches the format of the ADDR message
    var time: Long = 0.toLong();  // 32 bits on wire
    var services: ULong = 0.toULong()  // 64 bits on wire
    var port: Int = 0.toInt() // Short on wire
    var ip: ByteArray = ByteArray(16) // 16 bytes on wire

    // Disk only
    var keep: Boolean = false

    constructor(ipp: String, portp: Int, keepForever: Boolean = false) : this()
    {
        val inetAddr = InetAddress.getByName(ipp)
        ip = inetAddr.address
        port = portp
        keep = keepForever
    }

    // Serialization
    constructor(stream: BCHserialized) : this()
    {
        BCHdeserialize(stream)
    }

    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        // Network serialization must match ADDR message
        var ret = BCHserialized(format) + time + services + exactBytes(ip) + BCHserialized.uint16(port.toLong())
        if (format == SerializationType.DISK) ret.add(keep)
        return ret
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        // Network serialization must match ADDR message
        time = stream.deuint32().toLong()
        services = stream.deuint64().toULong()
        ip = stream.debytes(16)
        port = stream.deBigEndianUint16().toInt()
        if (stream.format == SerializationType.DISK)
            keep = stream.deboolean()
        else keep = false
        return stream
    }

    fun ipAsString(): String
    {
        val inet = InetAddress.getByAddress(ip)
        val nodeIp: String = inet.hostAddress
        return nodeIp
    }

    fun endpoint(): String
    {
        return ipAsString() + ":" + port.toString()
    }
}

@ExperimentalTime
@ExperimentalUnsignedTypes
@cli(Display.Simple, "Manage connections into the blockchain network")
class MultiNodeCnxnMgr(override val chainName: String, val netType: ChainSelector, seed: Array<String>) : CommonCnxnMgr()
{
    val dataLock = ThreadCond()
    val prospects: MutableMap<String, PossibleConnection> = mutableMapOf()
    var curProspect = 0

    /** If a seeder was supplied, store its domain name here */
    var originalDnsSeeds: Array<String>? = null

    /** Active seeders */
    var dnsSeederAddress: Array<String>? = null

    /** Maintain this many connections to the bitcoin network */
    @cli(Display.Simple, "Maintain this many connections to the bitcoin network")
    var desiredConnectionCount = 4

    /** This connection is coming up */
    var initializingCnxns: MutableList<BCHp2pClient> = mutableListOf()

    @cli(Display.Simple, "Only connect to this node.  Use null for non-exclusive mode")
    var exclusiveNodeName: String? = null

    @cli(Display.Simple, "If an exclusive node is set, use this port")
    var exclusiveNodePort: Int = BlockchainPort[netType]!!

    @cli(Display.Simple, "Always try connecting to this node, but also connect to other nodes.")
    var preferredNodeName: String? = null

    @cli(Display.Simple, "Preferred node's port")
    var preferredNodePort: Int = BlockchainPort[netType]!!

    init // parse seed and if its an IP add it to the list of possible nodes
    {
        launch { // Make async to speed up initialization
            for (s in seed)
            {
                val ipAndPort = SplitIpPort(s, BlockchainPort[netType]!!)

                try
                {
                    val pc = PossibleConnection(ipAndPort.first, ipAndPort.second, true)  // Keep any explicitly configured addresses forever in the possible connection list
                    synchronized(dataLock) { prospects[pc.endpoint()] = pc }
                }
                catch (e: Exception)  // Can get UnknownHostException or SecurityException
                {
                    // nothing to do
                }
            }
        }

        originalDnsSeeds = seed
        dnsSeederAddress = originalDnsSeeds?.clone()
    }

    /** Returns true if we are connected to this node/port */
    @cli(Display.User, "Returns true if we are connected to this node/port")
    override fun connectedTo(ip: String, port: Int): Boolean
    {
        for (c in initializingCnxns)
        {
            if ((c.port == port) && (c.name == ip)) return true
        }
        return super.connectedTo(ip, port)
    }

    /** report a node as having a problem */
    @cli(Display.Dev, "Report a node as having a problem")
    override fun misbehaving(cnxn: BCHp2pClient, reason: BanReason, count: Int)
    {
        if (count >= 100)
        {
            ban(cnxn.name, BanData(reason))
            LogIt.warning(sourceLoc() + " " + cnxn.logName + ": Disconnecting, misbehaving count is too large")
            cnxn.close()
        }
        launch { report(cnxn) }
    }

    @cli(Display.Dev, "Do not connect to this node again")
    fun ban(nodeIp: String, bd: BanData)
    {
        if (prospects[nodeIp]?.keep == true) return // Don't ban a hard-coded node
        synchronized(bannedLock)
        {
            bannedNodes[nodeIp] = bd
        }
    }

    /** wake up and analyze my connections, especially the optional passed connection */
    override suspend fun coReport(problemCnxn: BCHp2pClient?)
    {
        wakingDelay.wake()
    }

    /** wake up and analyze my connections, especially the optional passed connection */
    override fun report(problemCnxn: BCHp2pClient?)
    {
        wakingDelay.wake()
    }

    @cli(Display.User, "Add a new possible P2P connection into the manager")
    override fun add(ip: String, port: Int)
    {
        val pc = try
        {
            PossibleConnection(ip, port)
        }
        catch (e: UnknownHostException)
        {
            return
        }
        synchronized(dataLock) { prospects[pc.endpoint()] = pc }
    }

    /** Given a name and port, return the connection if we have one, or null if we don't.  This includes pending connections */
    @cli(Display.Dev, "Given a name and port, return the connection if we have one, or null if we don't.  This includes pending connections")
    fun find(name: String, port: Int): BCHp2pClient?
    {
        val p = try
        {
            PossibleConnection(name, port)
        }
        catch (e: UnknownHostException)
        {
            return null
        }
        val ips = p.ipAsString()
        synchronized(cnxnLock)
        {

            for (c in cnxns)
            {
                // catch both FQDNs and IPs
                if (((c.name == name) || (c.name == ips)) && (c.port == port)) return c
            }
            for (c in initializingCnxns)
            {
                if (((c.name == name) || (c.name == ips)) && (c.port == port)) return c
            }
        }
        return null
    }

    @cli(Display.Simple, "Configure a preferred node.  Use null to remove an existing one.")
    override fun preferNode(ip: String?, port: Int)
    {
        if (ip == null)
        {
            synchronized(cnxnLock)
            {
                preferredNodeName = null
            }
        }
        else
        {
            synchronized(cnxnLock)
            {
                preferredNodeName = ip
                preferredNodePort = port
                // its a network operation to resolve the name
                launch { preferredCnxn = find(ip, port) }  // If we happen to already be connected to it, then great
            }
        }
    }

    @cli(Display.Simple, "Configure an exclusive node.  Use null to remove an existing one.")
    override fun exclusiveNode(ip: String?, port: Int)
    {
        if (ip == null)
        {
            synchronized(cnxnLock)
            {
                exclusiveNodeName = null
            }
        }
        else
        {
            LogIt.warning(sourceLoc() + chainName + ": Disconnecting, exclusive node mode")
            synchronized(cnxnLock)
            {
                if (!((exclusiveNodeName == ip) && (exclusiveNodePort == port)))  // already set
                {
                    exclusiveNodeName = ip
                    exclusiveNodePort = port
                    // wipe out anything I'm trying to connect to
                    for (i in initializingCnxns)
                    {
                        if ((i.port == port) && (i.name == ip)) continue
                        i.close()
                    }
                    // wipe out any current connections (except to the chosen node)
                    for (i in cnxns)
                    {
                        if ((i.port == port) && (i.name == ip)) continue
                        i.close()
                    }
                }
            }
        }
    }

    /** Returns a possible peer from our prospects list */
    fun getProspectivePeer(): PossibleConnection
    {
        synchronized(dataLock) {
            curProspect++
            val tmp = prospects.entries  // TODO: likely very inefficient to convert the map to a set every time
            return tmp.elementAtOrElse(curProspect, {
                curProspect = 0
                tmp.elementAtOrElse(0) {
                    throw P2PNoNodesException()
                }
            }).value
        }
    }

    /** Call if this connection didn't work out */
    fun deleteProspectivePeer(p: PossibleConnection)
    {
        synchronized(dataLock) {
            if ((prospects.size > 0) && (p.keep == false))  // In the case where we've configured just 1 connection, better to not remove it for any reason.
                prospects.remove(p.endpoint())
        }
    }

    var curElectrum: Int = 0

    /** Returns a random connection from the connection manager's node set */
    @cli(Display.Simple, "Get a electrum connection")
    override fun getElectrum(): ElectrumClient
    {
        if (ecnxns.size > 0)
        {
            curElectrum += 1
            return ecnxns[curElectrum % ecnxns.size]
        }
        else
        {
            while (true)
            {
                val get = getElectrumServerCandidate?.invoke(netType)
                if (get == null) throw ElectrumNoNodesException()

                val ec = try
                {
                    ElectrumClient(netType, get.ip, get.port, autostart = false)
                }
                catch (e: java.net.ConnectException)
                {
                    continue  // Try another one
                }
                catch (e: java.net.UnknownHostException)
                {
                    continue
                }
                ec.start()  // OK we got one
                ecnxns.add(ec)
                return ec
            }
        }
    }

    /** Returns a random connection from the connection manager's node set, but not one of these.
     * Returns null if no connections left */
    @cli(Display.Simple, "Returns a random connection from the connection manager's node set, but not one of the passed set.  Returns null if no connections left")
    override fun getAnotherElectrum(notThese: Set<ElectrumClient>): ElectrumClient?
    {
        throw NotImplementedError()
    }

    @kotlin.ExperimentalUnsignedTypes
    fun processAddrMessage(newPeers: MutableList<PossibleConnection>, source: BCHp2pClient)
    {
        if (!MY_CNXNS_ONLY)  // debugging intervention to stop finding connections to external nodes
        {
            synchronized(dataLock) {
                for (p in newPeers)
                {
                    prospects[p.endpoint()] = p
                }
            }
        }
    }

    @ExperimentalTime
    fun cleanupConnections()
    {
        var i = 0
        var removedCxn: BCHp2pClient? = null
        synchronized(cnxnLock) {
            while (i < cnxns.size)
            {
                try
                {
                    val c = cnxns[i]
                    if ((c.sock.isConnected == false) || c.sock.isClosed() || c.sock.isInputShutdown() || c.sock.isOutputShutdown() || (c.lastReceiveTime.elapsedNow().inWholeSeconds > MAX_QUIET_TIME_SEC))
                    {
                        LogIt.info(sourceLoc() + " " + c.logName + ": Disconnecting, cleanup closed connection")
                        report(c)
                        cnxns.removeAt(i)
                        removedCxn = c
                    }
                    else
                    {
                        i++
                    }
                }
                catch (e: P2PDisconnectedException)
                {
                    initializingCnxns.removeAt(i)
                }
                catch (e: java.lang.ArrayIndexOutOfBoundsException)
                {
                    break
                }
                catch (e: java.lang.IndexOutOfBoundsException)
                {
                    break
                }
            }
        }
        // Invoke the connection change outside of the lock since we don't know what the registered fns do.
        removedCxn?.let { changeCallback?.invoke(this, it) }
        removedCxn = null

        i = 0
        while (i < initializingCnxns.size)
        {
            try
            {
                synchronized(cnxnLock)
                {
                    val c = initializingCnxns[i]
                    if ((c.beginTime.elapsedNow().inWholeMilliseconds > BCHp2pClient.CONNECT_TIMEOUT + BCHp2pClient.INITIAL_MESSAGE_XCHG_TIMEOUT))
                    {
                        LogIt.warning(sourceLoc() + " " + initializingCnxns[i].logName + ": Disconnecting, initialization timeout")
                        c.close()
                        initializingCnxns.removeAt(i)
                    }
                    else if ((c.sock.isConnected == false) || c.sock.isClosed() || c.sock.isInputShutdown() || c.sock.isOutputShutdown())
                    {
                        LogIt.warning(sourceLoc() + " " + initializingCnxns[i].logName + ": Disconnecting, drop during initialization")
                        c.close()
                        initializingCnxns.removeAt(i)
                    }
                    else
                    {
                        i++
                    }
                    0
                }
            }
            catch (e: P2PDisconnectedException)
            {
                LogIt.warning(sourceLoc() + " " + initializingCnxns[i].logName + ": Disconnecting, disconnect")
                initializingCnxns.removeAt(i)
            }
            catch (e: java.lang.ArrayIndexOutOfBoundsException)
            {
                break
            }
            catch (e: java.lang.IndexOutOfBoundsException)
            {
                break
            }
        }

        try
        {
            val c = preferredCnxn
            if ((c != null) && ((c.sock.isConnected == false) || c.sock.isClosed() || c.sock.isInputShutdown() || c.sock.isOutputShutdown() || (c.lastReceiveTime.elapsedNow().inWholeSeconds > MAX_QUIET_TIME_SEC)))
            {
                report(c)
                preferredCnxn = null
                changeCallback?.invoke(this, c)

            }
        }
        catch (e: P2PDisconnectedException)
        {
            preferredCnxn?.let { changeCallback?.invoke(this, it) }
            preferredCnxn = null
        }
    }

    @kotlin.ExperimentalUnsignedTypes
    fun initialConnect(prospect: PossibleConnection, cnxn: BCHp2pClient)
    {
        val nodeIpPort: String = prospect.endpoint()
        var cnx: BCHp2pClient? = null
        try
        {
            LogIt.info(sourceLoc() + " " + cnxn.logName + ": Connecting to " + nodeIpPort)
            cnxn.connect()
            cnx = cnxn
            cnxn.onInvCallback.add({ a, b -> launch { processInvMessage(a, b) } })
            cnxn.onTxCallback.add({ a, b -> processTxMessage(a, b) })
            cnxn.onBlockCallback.add({ a, b -> processBlockMessage(mutableListOf(a), b) })
            cnxn.onMerkleBlockCallback.add({ a, b -> processMerkleBlockMessage(mutableListOf(a), b) })
            cnxn.onHeadersCallback.add({ a, b -> processHeaderMessage(a, b); false })
            cnxn.onAddrCallback.add({ a, b -> processAddrMessage(a, b) })

            try
            {
                cnxn.waitForReady()
                cnxn.sendGetAddr()  // for any new connection, let's get who it knows about
                addressBloomFilter?.let {
                    cnxn.sendBloomFilter(it)
                }
                // Don't add the connection into the general pool until its ready
                // This is very important because many messages (like bloom filter install) are ignored during the initial message exchange,
                // and may even screw it up (node will assume that BU XVERSION is not coming if other messages arrive).
                synchronized(cnxnLock)
                {
                    if (cnxn.isAlive())
                    {
                        LogIt.info(sourceLoc() + ": Added connection " + cnxn.logName)
                        cnxns.add(cnxn)
                    }
                }
                changeCallback?.invoke(this, cnxn)
            }
            catch (e: P2PDisconnectedException)  // Connection worked by responding too slowly
            {
                LogIt.info(sourceLoc() + " Banning node on connect attempt " + cnxn.logName + " because " + e.toString() + "\n")
                cnxn.close()
                ban(nodeIpPort, BanData(BanReason.TOO_SLOW))
                deleteProspectivePeer(prospect)
            }
        }
        catch (e: java.net.ConnectException)
        {
            LogIt.info(sourceLoc() + " " + cnxn.logName + ": Connection not established, " + e.toString())
            cnxn.close()
            ban(nodeIpPort, BanData(BanReason.CANNOT_CONNECT))
            deleteProspectivePeer(prospect)

        }
        catch (e: java.net.SocketTimeoutException)
        {
            LogIt.info(sourceLoc() + " " + cnxn.logName + ": Disconnecting, " + e.toString())
            cnxn.close()
            ban(nodeIpPort, BanData(BanReason.TOO_SLOW))
            deleteProspectivePeer(prospect)
        }

        synchronized(cnxnLock)
        {
            // Worked or failed, initialization is done
            if (cnx != null) initializingCnxns.remove(cnx)
        }
    }

    @ExperimentalTime
    fun reassessBannedNodes()
    {
        synchronized(bannedLock)
        {
            val it = bannedNodes.iterator()
            for (d in it)
            {
                // Retry too slow connections whenever we need nodes
                if (d.value.reason == BanReason.TOO_SLOW) it.remove()
                // Retry these every 5 minutes -- server may have been restarted
                if ((d.value.reason == BanReason.EARLY_DISCONNECT) && (d.value.start.elapsedNow().inWholeSeconds > 300)) it.remove()
                if ((d.value.reason == BanReason.CANNOT_CONNECT) && (d.value.start.elapsedNow().inWholeSeconds > 300)) it.remove()
            }

        }
    }

    fun cnxnProcessor()
    {
        while (!done)
        {
            val cxn = mutableListOf<BCHp2pClient>()
            synchronized(cnxnLock)
            {
                cxn.addAll(cnxns)
                cxn.addAll(initializingCnxns)
            }
            var didSomething = 0
            for (c in cxn)
                try
                {
                    val tmp = c.process()
                    didSomething = didSomething or if (tmp) 1 else 0
                }
                catch (e: java.lang.Exception)
                {
                    LogIt.warning(sourceLoc() + " " + chainName + ": Unexpected Exception: " + e.toString())
                    var stkData = ByteArrayOutputStream()
                    var stkStream = PrintStream(stkData)
                    e.printStackTrace(stkStream)
                    stkStream.flush()
                    LogIt.warning(String(stkData.toByteArray()))
                    wakingDelay.delay(500)
                }
            if (didSomething == 0) Thread.sleep(20)
        }
    }

    /** disconnect from the lowest N performing peers */
    @cli(Display.Simple, "disconnect from the lowest N performing peers")
    fun dropWorstConnections(count: Int)
    {
        for (j in 0 until count)
        {
            // TODO use connection stats to pick a connection
            var removedCxn: BCHp2pClient?
            synchronized(cnxnLock)
            {
                val i = cnxns.size - 1
                cnxns[i].close()
                removedCxn = cnxns[i]
                LogIt.info(sourceLoc() + ": Disconnecting, worst connection")
                cnxns.removeAt(i)
            }
            removedCxn?.let { changeCallback?.invoke(this, it) }
        }
    }

    override fun run()
    {
        thread(true, true, null, chainName + "Cnxns") { cnxnProcessor() }
        while (!done)
        {
            try
            {
                if (dnsSeederAddress?.size == 0) dnsSeederAddress = originalDnsSeeds  // If I eliminate them all, try them all again
                cleanupConnections()
                // If we aren't connected to our preferred node, drop someone
                val pref = preferredNodeName
                var needPrefConnect = if (pref == null)
                    false
                else
                {
                    if (preferredCnxn == null) true
                    else false
                }

                if (needPrefConnect && ((synchronized(cnxnLock) { cnxns.size }) >= desiredConnectionCount))
                {
                    dropWorstConnections(1)
                }

                val csize = synchronized(cnxnLock) { cnxns.size }
                val enp = exclusiveNodePort
                val enn = exclusiveNodeName
                if (enn != null)
                {
                    if ((csize == 0) && (initializingCnxns.size == 0))
                    {
                        try
                        {
                            val prospect = PossibleConnection(enn, enp)
                            val nodeIp: String = prospect.ipAsString()
                            val nodePort = prospect.port
                            val cnxn = BCHp2pClient(netType, nodeIp, nodePort.toInt(), chainName + "@" + nodeIp + ":" + nodePort, coScope, cocond)
                            initializingCnxns.add(cnxn)
                            thread(true, true, null, "connect_${prospect.ipAsString()}") { initialConnect(prospect, cnxn) }
                        }
                        catch (e: UnknownHostException)  // not much to do if our exclusive host doesn't exist
                        {
                            LogIt.info("unknown host")
                            wakingDelay.delay(5000)
                        }
                    }
                }

                if ((enn == null) && (csize < desiredConnectionCount) && (initializingCnxns.size < SIMULTANEOUS_CNXN_INIT))
                {
                    try     // Reconnect to a node
                    {
                        val CnxnsToInit = min(desiredConnectionCount.toInt() - csize, SIMULTANEOUS_CNXN_INIT.toInt() - initializingCnxns.size.toInt())
                        for (i in 0 until CnxnsToInit)
                        {
                            var delay = 0L
                            synchronized(cnxnLock)
                            {
                                var prospect: PossibleConnection? = null
                                var preferred = false

                                if (needPrefConnect)
                                {
                                    try
                                    {
                                        prospect = PossibleConnection(pref!!, preferredNodePort)
                                        preferred = true
                                    }
                                    catch (e: UnknownHostException)  // drop thru with p == null
                                    {
                                        preferNode(null, preferredNodePort)  // turn off preferential connection if the host doesn't map or we'll keep kicking a node to reserve space and then using it for a non-preferential node
                                    }
                                }

                                if (prospect == null)
                                {

                                    for (count in 0..5)  // Loop to find a node that's not banned and we aren't connect to.  Try a couple per loop
                                    {
                                        val tmp = getProspectivePeer()
                                        if (!connectedTo(tmp.ipAsString(), tmp.port.toInt()))
                                            if (synchronized(bannedLock) { !bannedNodes.contains(tmp.endpoint()) })
                                            {
                                                prospect = tmp
                                                break
                                            }
                                        deleteProspectivePeer(tmp)
                                    }
                                }

                                // If we got something, connect
                                if (prospect != null)
                                {
                                    val nodeIp: String = prospect.ipAsString()
                                    val nodePort = prospect.port.toInt()

                                    if (initializingCnxns.filter { (it.name == nodeIp && it.port == prospect.port) }.size == 0)
                                    {
                                        val cnxn = BCHp2pClient(netType, nodeIp, nodePort, chainName + "@" + prospect.endpoint(), coScope, cocond)
                                        initializingCnxns.add(cnxn)
                                        if (preferred) preferredCnxn = cnxn
                                        thread(true, true, null, "connect_${prospect.ipAsString()}") { initialConnect(prospect, cnxn) }
                                        needPrefConnect = false // Once through would have picked the preferred if it is available
                                    }
                                    else
                                    {
                                        delay = 1000L // If I'm already trying this prospect, slow down the retry
                                    }
                                }
                                else  // If I don't have new prospects, then slow down the retry
                                {
                                    delay = 2000L
                                }
                            }
                            if (delay > 0) wakingDelay.delay(delay)
                        }
                    }
                    catch (e: UnknownHostException)
                    {
                        LogIt.info("unknown host")
                    }
                    catch (e: P2PNoNodesException)
                    {
                        val seed = dnsSeederAddress
                        if (seed != null)
                        {
                            for (s in seed)
                            {
                                try
                                {
                                    // Fill the prospects with some data from the seeder DNS
                                    withPeersFrom(s) {
                                        for (inet in it)
                                        {
                                            val pc = PossibleConnection(inet.hostAddress, (BlockchainPort[netType] ?: BCHregtest2Port).toInt())
                                            synchronized(dataLock) { prospects[pc.endpoint()] = pc }
                                        }
                                    }
                                }
                                catch (e: UnknownHostException)
                                {
                                    LogIt.warning(sourceLoc() + " " + chainName + ": " + s + " is unknown or offline")
                                    // Get rid of this seeder if its offline
                                    if (iHaveInternet())
                                        dnsSeederAddress = dnsSeederAddress?.filterNot { it == s }?.toTypedArray()
                                }
                            }
                        }
                        reassessBannedNodes()
                    }
                }

                synchronized(cnxnLock)
                {
                    for (cnxn in cnxns)  // Periodic maintenance
                    {
                        try
                        {

                            if ((cnxn.lastReceiveTime.elapsedNow().inWholeSeconds > PING_QUIET_TIME_SEC) && (cnxn.pingOutstanding == false))
                            {
                                LogIt.info(sourceLoc() + " " + cnxn.logName + ": ping ")
                                cnxn.lastPingCount += 1
                                cnxn.pingOutstanding = true
                            }
                            if ((bloomCount != 0) && (cnxn.bloomCount != bloomCount))
                            {
                                LogIt.info(sourceLoc() + " " + cnxn.logName + ": periodic bloom update")
                                addressBloomFilter?.let { cnxn.sendBloomFilter(it, bloomCount) }
                            }
                        }
                        catch (e: IndexOutOfBoundsException)  // Happens if another thread deleted a connection under us, ignore
                        {

                        }
                        catch (e: P2PDisconnectedException)
                        {
                            LogIt.info(sourceLoc() + " " + cnxn.logName + ": Disconnecting, " + e.message)
                            cnxn.close()
                        }

                    }
                }

                wakingDelay.delay(500)
            }
            catch (e: TimeoutCancellationException)
            {
                // nothing to do if various requests timed out. just loop around and try again
            }
            catch (e: java.lang.Exception)
            {
                handleThreadException(e, sourceLoc() + " " + chainName + ": ")
            }
        }
    }
}


open class RegTestCnxnMgr(override val chainName: String, val nodeIp: String, val nodePort: Int) : CommonCnxnMgr()
{
    /** wake up the processing loop */
    val wakey = ThreadCond()

    override fun add(ip: String, port: Int)
    {
        LogIt.info("Not adding the potential peer $ip:$port to a regtest blockchain")
    }


    public override fun exclusiveNode(ip: String?, port: Int): Unit
    {
        LogIt.info("Not adding the exclusive node $ip:$port to a regtest blockchain")
    }

    public override fun preferNode(ip: String?, port: Int): Unit
    {
        LogIt.info("Not adding the preferred node $ip:$port to a regtest blockchain")
    }

    /** report a node as having a problem */
    override fun misbehaving(cnxn: BCHp2pClient, reason: BanReason, count: Int)
    {
        LogIt.warning("Connection to regtest was indicated as misbehaving because: ${reason.name}")
    }

    // In the regtest mode, we just have to keep giving the same nodes
    override fun getAnotherNode(notThese: Set<BCHp2pClient>): BCHp2pClient?
    {
        var tries = 0
        while (true)
        {
            tries += 1
            if (tries > cnxns.size) return null // I've tried everything in cnxns
            if (cnxns.size > 0)
            {

                val c = grabAndCheckConnection()
                if (c != null)
                {
                    return c
                }
            }
            else wakingDelay.delay(OUT_OF_NODES_DELAY)
        }
    }

    override fun run()
    {
        while (!done)
        {
            var delay = 500
            for (i in 0..cnxns.size - 1)  // clean up dead connections
            {
                val c = cnxns[i]
                if ((c.sock.isConnected == false) || (c.sock.isClosed() || c.sock.isInputShutdown() || c.sock.isOutputShutdown()))
                {
                    LogIt.warning(sourceLoc() + ": Disconnecting, connection is dropped")
                    cnxns.removeAt(i)
                    changeCallback?.invoke(this, c)
                }
            }

            if (cnxns.count() == 0)
            {
                try     // Reconnect to a node
                {
                    LogIt.info(sourceLoc() + " " + chainName + ": Connecting to " + nodeIp + ":" + nodePort.toString())
                    var cnxn = BCHp2pClient(ChainSelector.BCHREGTEST, nodeIp, nodePort, chainName + "@" + nodeIp + ":" + nodePort, coScope, cocond).connect()

                    cnxn.onInvCallback.add({ a, b -> launch { processInvMessage(a, b) } })
                    cnxn.onTxCallback.add({ a, b -> processTxMessage(a, b) })
                    cnxn.onBlockCallback.add({ a, b -> processBlockMessage(mutableListOf(a), b) })
                    cnxn.onMerkleBlockCallback.add({ a, b -> processMerkleBlockMessage(mutableListOf(a), b) })
                    cnxn.onHeadersCallback.add({ a, b -> processHeaderMessage(a, b); false })

                    thread(true, true, null, nodeIp + ":" + nodePort + "_msgrecv") { cnxn.processForever() }
                    runBlocking { cnxn.waitForReady() }
                    addressBloomFilter?.let {
                        cnxn.sendBloomFilter(it)
                    }
                    // Don't add the connection into the general pool until its ready
                    // This is very important because many messages (like bloom filter install) are ignored during the initial message exchange,
                    // and may even screw it up (node will assume that BU XVERSION is not coming if other messages arrive).
                    cnxns.add(cnxn)
                    changeCallback?.invoke(this, cnxn)
                }
                catch (e: java.net.ConnectException)
                {
                    LogIt.info(e.toString())
                    delay = 15000  // In regtest there's only 1 node to connect to, so don't spam reconnect if something's going wrong
                }
                catch (e: java.net.SocketTimeoutException)
                {
                    LogIt.info(e.toString())
                    delay = 15000 // In regtest there's only 1 node to connect to, so don't spam reconnect if something's going wrong
                }
                catch (e: P2PDisconnectedException)  // Connection worked by responding too slowly
                {
                    LogIt.info(e.toString())
                    delay = 15000 // In regtest there's only 1 node to connect to, so don't spam reconnect if something's going wrong
                }

            }

            for (i in 0..cnxns.size - 1)  // Periodic maintenance
            {
                if ((bloomCount != 0) && (cnxns[i].bloomCount != bloomCount))
                {
                    LogIt.info(sourceLoc() + " " + chainName + ": periodic bloom update")
                    addressBloomFilter?.let { cnxns[i].sendBloomFilter(it, bloomCount) }
                }

            }
            wakey.delay(delay.toLong())
        }


    }
}

