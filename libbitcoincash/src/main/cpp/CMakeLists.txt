# For more information about using CMake with Android Studio, read the
# documentation: https://d.android.com/studio/projects/add-native-code.html

cmake_minimum_required(VERSION 3.13.0)
project(CashLib)
# Build bucashlib aka "libbitcoincashandroid" from the Bitcoin Unlimiteds
# repository
set(contrib_dir ${CMAKE_CURRENT_LIST_DIR}/../../../../contrib)
set(srcdir ${contrib_dir}/build/BU/BCHUnlimited/src/cashlib)
set(builddir obj)

find_package (Python3 COMPONENTS Interpreter)

option(ENABLE_RINGSIG "Enable experimental traceable ring signature support" OFF)

if (NOT ((EXISTS ${srcdir}/CMakeLists.txt) AND (EXISTS ${srcdir}/../../configure)))
    message(STATUS "Downloading and preparing libbitcoincash source code")
    file(MAKE_DIRECTORY ${srcdir})
    execute_process(
        COMMAND ${PYTHON_EXECUTABLE} ${contrib_dir}/prepare-libbitcoincash-sources.py
        RESULT_VARIABLE rv
        OUTPUT_VARIABLE out
        ERROR_VARIABLE err)

    if (NOT rv EQUAL "0")
        message(STATUS out)
        message(STATUS err)
        message(FATAL_ERROR "${out} ${err}\n\nFailed to prepare libbitcoincash source code. Exit status ${rv}")
    endif()
endif()

add_subdirectory( # Specifies the directory of the CMakeLists.txt file.
                  ${srcdir}
                  # Specifies the directory for the build outputs.
                  ${builddir} )

if (ENABLE_RINGSIG)
    add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/ringsigandroid)
endif()
