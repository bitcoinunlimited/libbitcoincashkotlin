package info.bitcoinunlimited.libbitcoincash

import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.Initialize
import bitcoinunlimited.libbitcoincash.UtilStringEncoding
import bitcoinunlimited.libbitcoincash.UtilVote
import bitcoinunlimited.libbitcoincash.vote.RingSignatureVoteBase
import com.google.common.truth.Truth.assertThat
import org.junit.Test

class RingSignatureVoteBaseTest {
    companion object {
        init {
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(ChainSelector.BCHMAINNET.v)
        }
    }

    class TestElection {
        companion object {
            val salt = "51f20e3f84998a41bef6b768394726d0403c935c046f3d53e494f1553b442ea6".toByteArray()
            val description = "Anon"
            val beginHeight = 700818
            val endHeight = 709755
            val voteOptionsHashed = arrayOf(
                UtilVote.hash160Salted(salt, "A".toByteArray()),
                UtilVote.hash160Salted(salt, "B".toByteArray())
            )
            val participants = arrayOf(
                // bitcoincash:qrq08q63ymlxn95f9ptyq65jjwekzsdl2gp8rde6j0
                UtilStringEncoding.hexToByteArray(
                    "a03d120c227b9c8cbe9fa3775b377358fcd8a756c45fedfa5c50bcb0cdb45864"
                ),
                // bitcoincash:qqj3uzc9f8uzqgrdjhj609zqcdaj2h228uf9fae79k
                UtilStringEncoding.hexToByteArray(
                    "6edcf341f28bba48e7701c93a60917f640b98b180110d09c5fa4ef24c5affc55"
                )
            )
        }
    }

    @Test
    fun calculateElectionId() {
        val electionID = RingSignatureVoteBase.calculateElectionId(
            TestElection.salt,
            TestElection.description,
            TestElection.beginHeight,
            TestElection.endHeight,
            TestElection.voteOptionsHashed,
            TestElection.participants
        )
        assertThat(UtilStringEncoding.toHexString(electionID))
            .isEqualTo("d231b87923ed25b9a536a5d0f3ed4b18c038406d")
    }

    @Test
    fun getNotificationAddress() {
        val electionID = RingSignatureVoteBase.calculateElectionId(
            TestElection.salt,
            TestElection.description,
            TestElection.beginHeight,
            TestElection.endHeight,
            TestElection.voteOptionsHashed,
            TestElection.participants
        )
        val notificationAddress = RingSignatureVoteBase.getNotificationAddress(
            ChainSelector.BCHMAINNET, electionID
        )
        assertThat(notificationAddress.toString()).isEqualTo(
            "bitcoincash:prvsjxtfygelawad7t87t2rj8rr5u87vuu5sek8jp6"
        )
    }
}