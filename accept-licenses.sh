#!/usr/bin/env bash
set -e
if [ -z "$ANDROID_SDK_ROOT" ]; then
    echo "Error: ANDROID_SDK_ROOT env variable not set."
    false
fi

yes | $ANDROID_SDK_ROOT/bin/sdkmanager --licenses --sdk_root=$ANDROID_SDK_ROOT
